# Overview

This project is about an unofficial online character sheet/creator for the [Earthdawn RPG](https://en.wikipedia.org/wiki/Earthdawn). It allows to create and level a character and save/restore the progress as JSON file.
It is mainly Html/Javascript based and runs in a normal web browser. The necessary game data is provided via http requests from a server (via PHP and an SQL database) but could easily be included directly in the Javascript source instead.

So far the sheet supports only the basic attributes, disciplines, races, skills and talents. Spells, equipment, magic items, etc. are not covered.
It was created as a past-time project for a private group and thus uses a wild mixture of 1st and (mainly) 2nd edition rules with some ideas borrowed from 3rd edition.
For that reason it is unlikely that you can use the data/code as it is for your group without modifications.

You can see the character sheet in action here: https://www.behindthemirrors.de/earthdawn/character.html
