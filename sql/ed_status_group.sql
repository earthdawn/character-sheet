CREATE TABLE `ed_status_group` (
  `id` tinyint(1) unsigned NOT NULL auto_increment,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


INSERT INTO `ed_status_group` VALUES (1, 'Initiate');
INSERT INTO `ed_status_group` VALUES (2, 'Novice');
INSERT INTO `ed_status_group` VALUES (3, 'Journeyman');
INSERT INTO `ed_status_group` VALUES (4, 'Warden');
INSERT INTO `ed_status_group` VALUES (5, 'Master');
