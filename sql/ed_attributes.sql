CREATE TABLE `ed_attributes` (
  `id` tinyint(1) unsigned NOT NULL auto_increment,
  `name` varchar(10) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=7 ;


INSERT INTO `ed_attributes` VALUES (1, 'Dexterity');
INSERT INTO `ed_attributes` VALUES (2, 'Strength');
INSERT INTO `ed_attributes` VALUES (3, 'Toughness');
INSERT INTO `ed_attributes` VALUES (4, 'Perception');
INSERT INTO `ed_attributes` VALUES (5, 'Willpower');
INSERT INTO `ed_attributes` VALUES (6, 'Charisma');
