CREATE TABLE `ed_durability` (
  `discipline_id` tinyint(3) unsigned NOT NULL,
  `unconsciousness` tinyint(1) unsigned NOT NULL default '5',
  `death` tinyint(1) unsigned NOT NULL default '6',
  PRIMARY KEY  (`discipline_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


INSERT INTO `ed_durability` VALUES (1, 5, 6);
INSERT INTO `ed_durability` VALUES (2, 5, 6);
INSERT INTO `ed_durability` VALUES (3, 6, 7);
INSERT INTO `ed_durability` VALUES (4, 6, 7);
INSERT INTO `ed_durability` VALUES (5, 3, 4);
INSERT INTO `ed_durability` VALUES (6, 3, 4);
INSERT INTO `ed_durability` VALUES (7, 3, 4);
INSERT INTO `ed_durability` VALUES (8, 5, 6);
INSERT INTO `ed_durability` VALUES (9, 6, 8);
INSERT INTO `ed_durability` VALUES (10, 6, 7);
INSERT INTO `ed_durability` VALUES (11, 4, 5);
INSERT INTO `ed_durability` VALUES (12, 5, 6);
INSERT INTO `ed_durability` VALUES (13, 7, 9);
INSERT INTO `ed_durability` VALUES (14, 5, 6);
INSERT INTO `ed_durability` VALUES (15, 3, 4);
INSERT INTO `ed_durability` VALUES (16, 3, 4);
INSERT INTO `ed_durability` VALUES (17, 6, 8);
INSERT INTO `ed_durability` VALUES (18, 5, 6);
INSERT INTO `ed_durability` VALUES (19, 5, 6);
INSERT INTO `ed_durability` VALUES (20, 5, 6);
INSERT INTO `ed_durability` VALUES (21, 7, 9);
INSERT INTO `ed_durability` VALUES (22, 5, 6);
INSERT INTO `ed_durability` VALUES (23, 6, 7);
INSERT INTO `ed_durability` VALUES (24, 6, 7);
INSERT INTO `ed_durability` VALUES (25, 7, 9);
INSERT INTO `ed_durability` VALUES (26, 6, 7);
INSERT INTO `ed_durability` VALUES (27, 5, 6);
