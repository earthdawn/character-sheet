CREATE TABLE `ed_skills` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(40) character set utf8 collate utf8_unicode_ci NOT NULL,
  `action` tinyint(1) unsigned NOT NULL default '0',
  `strain` tinyint(2) unsigned NOT NULL default '0',
  `attribute` tinyint(1) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL default '1',
  `specialization` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 AUTO_INCREMENT=189 ;


INSERT INTO `ed_skills` VALUES (1, 'Embroidery', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (2, 'Calligraphy', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (3, 'Runic Carving', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (4, 'Wood Carving', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (5, 'Sculpture', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (6, 'Musician', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (7, 'Juggling', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (8, 'Storytelling', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (9, 'Acting', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (10, 'Poetry', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (11, 'Cooking', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (12, 'Body Painting', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (13, 'Painting', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (14, 'Dancing', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (15, 'Basket Weaving', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (16, 'Tattooing', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (17, 'Wardrobe and Style', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (18, 'Singing', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (19, 'Scarification', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (20, 'Craftsman', 1, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (21, 'Alchemy and Potions', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (22, 'Ancient Weapons', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (23, 'Barsaive History', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (24, 'Botany', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (25, 'Court Dances', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (26, 'Creature Lore', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (27, 'Dwarf Military Organization', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (28, 'Games of Chance', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (29, 'History of the Scourge', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (30, 'The Horrors', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (31, 'Legends and Heroes', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (32, 'Theran History', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (33, 'Theran Politics', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (34, 'Wild Animals', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (35, 'Map Making', 0, 0, 6, 3, NULL);
INSERT INTO `ed_skills` VALUES (36, 'Anatomy', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (37, 'Baking', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (38, 'Magic Lore', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (39, 'Military History', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (40, 'Military Procedures', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (41, 'Racial Lore: Blood Elf', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (42, 'Racial Lore: Dwarf', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (43, 'Racial Lore: Elf', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (44, 'Racial Lore: Human', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (45, 'Racial Lore: Obsidiman', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (46, 'Racial Lore: Ork', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (47, 'Racial Lore: Troll', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (48, 'Racial Lore: T''Skrang', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (49, 'Racial Lore: Windling', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (50, 'Subterranean Throal', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (51, 'Throal History', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (52, 'Weapon Lore', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (53, 'Acrobatics', 0, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (54, 'Air Sailing: Drakkar', 1, 0, 5, 1, 54);
INSERT INTO `ed_skills` VALUES (55, 'Air Sailing: Galley and Galleon', 1, 0, 5, 1, 54);
INSERT INTO `ed_skills` VALUES (56, 'Air Sailing: Vedette', 1, 0, 5, 1, 54);
INSERT INTO `ed_skills` VALUES (57, 'Air Sailing: Kila and Behemoth', 1, 0, 5, 1, 54);
INSERT INTO `ed_skills` VALUES (58, 'Animal Bond', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (59, 'Animal Handling', 1, 0, 5, 1, NULL);
INSERT INTO `ed_skills` VALUES (60, 'Animal Training', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (61, 'Anticipate Blow', 0, 1, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (62, 'Avoid Blow', 0, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (63, 'Battle Shout', 0, 1, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (64, 'Bribery', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (65, 'Charge', 0, 0, 2, 1, NULL);
INSERT INTO `ed_skills` VALUES (66, 'Climbing', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (67, 'Conceal Object', 1, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (68, 'Conversation', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (69, 'Detect Trap', 1, 1, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (70, 'Detect Weapon', 1, 1, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (71, 'Diplomacy', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (72, 'Disarm: Axes', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (73, 'Disarm: Clubs', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (74, 'Disarm: Flails', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (75, 'Disarm: Knives and Daggers', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (76, 'Disarm: Maces and Hammers', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (77, 'Disarm: Pole Arms', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (78, 'Disarm: Spears', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (79, 'Disarm: Staffs', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (80, 'Disarm: Swords', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (81, 'Disarm: Whips', 1, 1, 1, 1, 72);
INSERT INTO `ed_skills` VALUES (82, 'Disarm Trap', 1, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (83, 'Disguise', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (84, 'Engaging Banter', 1, 1, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (85, 'Etiquette', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (86, 'Evidence Analysis', 1, 1, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (87, 'Fast Hand', 1, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (88, 'Fence', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (89, 'First Impression', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (90, 'Fishing', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (91, 'Flirting', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (92, 'Forgery', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (93, 'Great Leap', 0, 1, 2, 1, NULL);
INSERT INTO `ed_skills` VALUES (94, 'Hiding', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (95, 'Haggle', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (96, 'Hunting', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (97, 'Impressive Shot: Bows', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (98, 'Impressive Shot: Crossbows', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (99, 'Impressive Shot: Slings', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (100, 'Impressive Shot: Blowguns', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (101, 'Impressive Shot: Darts and Spears', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (102, 'Impressive Shot: Hatchets and Axes', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (103, 'Impressive Shot: Knives and Daggers', 1, 1, 1, 1, 97);
INSERT INTO `ed_skills` VALUES (104, 'Lip Reading', 1, 2, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (105, 'Lock Picking', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (106, 'Maneuver', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (107, 'Melee Weapons: Axes', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (108, 'Melee Weapons: Clubs', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (109, 'Melee Weapons: Flails', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (110, 'Melee Weapons: Knives and Daggers', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (111, 'Melee Weapons: Maces and Hammers', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (112, 'Melee Weapons: Pole Arms', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (113, 'Melee Weapons: Spears', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (114, 'Melee Weapons: Staffs', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (115, 'Melee Weapons: Swords', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (116, 'Melee Weapons: Whips', 1, 0, 1, 1, 107);
INSERT INTO `ed_skills` VALUES (117, 'Missile Weapons: Blowguns', 1, 0, 1, 1, 117);
INSERT INTO `ed_skills` VALUES (118, 'Missile Weapons: Bows', 1, 0, 1, 1, 117);
INSERT INTO `ed_skills` VALUES (119, 'Missile Weapons: Crossbows', 1, 0, 1, 1, 117);
INSERT INTO `ed_skills` VALUES (120, 'Missile Weapons: Slings', 1, 0, 1, 1, 117);
INSERT INTO `ed_skills` VALUES (121, 'Momentum Attack', 0, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (122, 'Mimic Voice', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (123, 'Navigation', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (124, 'Parry: Axes', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (125, 'Parry: Clubs', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (126, 'Parry: Flails', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (127, 'Parry: Knives and Daggers', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (128, 'Parry: Maces and Hammers', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (129, 'Parry: Pole Arms', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (130, 'Parry: Spears', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (131, 'Parry: Staffs', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (132, 'Parry: Swords', 0, 1, 1, 1, 124);
INSERT INTO `ed_skills` VALUES (133, 'Physician', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (134, 'Pick Pockets', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (135, 'Read and Write Language', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (136, 'Pilot Boat', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (137, 'Research', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (138, 'Resist Taunt', 0, 1, 5, 1, NULL);
INSERT INTO `ed_skills` VALUES (139, 'Read and Write Magic', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (140, 'Sailing', 1, 0, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (141, 'Awareness', 0, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (142, 'Second Weapon: Axes', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (143, 'Second Weapon: Clubs', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (144, 'Second Weapon: Flails', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (145, 'Second Weapon: Knives and Daggers', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (146, 'Second Weapon: Maces and Hammers', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (147, 'Second Weapon: Pole Arms', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (148, 'Second Weapon: Spears', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (149, 'Second Weapon: Staffs', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (150, 'Second Weapon: Swords', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (151, 'Second Weapon: Whips', 0, 1, 1, 1, 142);
INSERT INTO `ed_skills` VALUES (152, 'Second Shot: Blowguns', 0, 1, 1, 1, 152);
INSERT INTO `ed_skills` VALUES (153, 'Second Shot: Bows', 0, 1, 1, 1, 152);
INSERT INTO `ed_skills` VALUES (154, 'Second Shot: Crossbows', 0, 1, 1, 1, 152);
INSERT INTO `ed_skills` VALUES (155, 'Second Shot: Slings', 0, 1, 1, 1, 152);
INSERT INTO `ed_skills` VALUES (156, 'Seduction', 1, 0, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (157, 'Shield Charge', 0, 1, 2, 1, NULL);
INSERT INTO `ed_skills` VALUES (158, 'Silent Walk', 0, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (159, 'Speak Language', 1, 0, 4, 4, NULL);
INSERT INTO `ed_skills` VALUES (160, 'Sprint', 0, 2, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (161, 'Streetwise', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (162, 'Surprise Strike: Axes', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (163, 'Surprise Strike: Clubs', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (164, 'Surprise Strike: Flails', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (165, 'Surprise Strike: Knives and Daggers', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (166, 'Surprise Strike: Maces and Hammers', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (167, 'Surprise Strike: Pole Arms', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (168, 'Surprise Strike: Spears', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (169, 'Surprise Strike: Staffs', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (170, 'Surprise Strike: Swords', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (171, 'Surprise Strike: Whips', 1, 1, 1, 1, 162);
INSERT INTO `ed_skills` VALUES (172, 'Swift Kick', 0, 2, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (173, 'Swimming', 1, 0, 2, 1, NULL);
INSERT INTO `ed_skills` VALUES (174, 'Tactics', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (175, 'Taunt', 1, 1, 6, 1, NULL);
INSERT INTO `ed_skills` VALUES (176, 'Throwing Weapons: Bolas and Nets', 1, 0, 1, 1, 176);
INSERT INTO `ed_skills` VALUES (177, 'Throwing Weapons: Darts and Spears', 1, 0, 1, 1, 176);
INSERT INTO `ed_skills` VALUES (178, 'Throwing Weapons: Hatchets and Axes', 1, 0, 1, 1, 176);
INSERT INTO `ed_skills` VALUES (179, 'Throwing Weapons: Knives and Daggers', 1, 0, 1, 1, 176);
INSERT INTO `ed_skills` VALUES (180, 'Tracking', 1, 1, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (181, 'Trap Initiative', 0, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (182, 'Trick Riding', 0, 1, 1, 1, NULL);
INSERT INTO `ed_skills` VALUES (183, 'Unarmed Combat: Grappling', 1, 0, 1, 1, 183);
INSERT INTO `ed_skills` VALUES (184, 'Unarmed Combat: Striking', 1, 0, 1, 1, 183);
INSERT INTO `ed_skills` VALUES (185, 'Unarmed Combat: Kicking', 1, 0, 1, 1, 183);
INSERT INTO `ed_skills` VALUES (186, 'Wilderness Survival', 1, 0, 4, 1, NULL);
INSERT INTO `ed_skills` VALUES (187, 'Caravans', 0, 0, 4, 2, NULL);
INSERT INTO `ed_skills` VALUES (188, 'Passions', 0, 0, 4, 2, NULL);
