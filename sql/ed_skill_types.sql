CREATE TABLE `ed_skill_types` (
  `id` tinyint(1) unsigned NOT NULL auto_increment,
  `name` varchar(10) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `ed_skill_types` VALUES (1, 'General');
INSERT INTO `ed_skill_types` VALUES (2, 'Knowledge');
INSERT INTO `ed_skill_types` VALUES (3, 'Artisan');
INSERT INTO `ed_skill_types` VALUES (4, 'Language');
