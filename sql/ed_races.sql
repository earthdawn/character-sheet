CREATE TABLE `ed_races` (
  `id` tinyint(2) unsigned NOT NULL auto_increment,
  `name` varchar(20) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;


INSERT INTO `ed_races` VALUES (1, 'Blood Elf');
INSERT INTO `ed_races` VALUES (2, 'Dwarf');
INSERT INTO `ed_races` VALUES (3, 'Elf');
INSERT INTO `ed_races` VALUES (4, 'Human');
INSERT INTO `ed_races` VALUES (5, 'Obsidiman');
INSERT INTO `ed_races` VALUES (6, 'Ork');
INSERT INTO `ed_races` VALUES (7, 'Troll');
INSERT INTO `ed_races` VALUES (8, 'T''Skrang');
INSERT INTO `ed_races` VALUES (9, 'Windling');
