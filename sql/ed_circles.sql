CREATE TABLE `ed_circles` (
  `circle` tinyint(2) unsigned NOT NULL auto_increment,
  `status_group_id` tinyint(1) unsigned NOT NULL,
  `core_talent_count` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY  (`circle`),
  KEY `status_group` (`status_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;


INSERT INTO `ed_circles` VALUES (1, 1, 5);
INSERT INTO `ed_circles` VALUES (2, 2, 2);
INSERT INTO `ed_circles` VALUES (3, 2, 1);
INSERT INTO `ed_circles` VALUES (4, 2, 1);
INSERT INTO `ed_circles` VALUES (5, 3, 1);
INSERT INTO `ed_circles` VALUES (6, 3, 1);
INSERT INTO `ed_circles` VALUES (7, 3, 1);
INSERT INTO `ed_circles` VALUES (8, 3, 1);
INSERT INTO `ed_circles` VALUES (9, 4, 2);
INSERT INTO `ed_circles` VALUES (10, 4, 1);
INSERT INTO `ed_circles` VALUES (11, 4, 1);
INSERT INTO `ed_circles` VALUES (12, 4, 1);
INSERT INTO `ed_circles` VALUES (13, 5, 1);
INSERT INTO `ed_circles` VALUES (14, 5, 1);
INSERT INTO `ed_circles` VALUES (15, 5, 1);
