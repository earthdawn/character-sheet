CREATE TABLE `ed_disciplines` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;


INSERT INTO `ed_disciplines` VALUES (1, 'Air Sailor');
INSERT INTO `ed_disciplines` VALUES (2, 'Archer');
INSERT INTO `ed_disciplines` VALUES (3, 'Beastmaster');
INSERT INTO `ed_disciplines` VALUES (4, 'Cavalryman');
INSERT INTO `ed_disciplines` VALUES (5, 'Elementalist');
INSERT INTO `ed_disciplines` VALUES (6, 'Illusionist');
INSERT INTO `ed_disciplines` VALUES (7, 'Nethermancer');
INSERT INTO `ed_disciplines` VALUES (8, 'Scout');
INSERT INTO `ed_disciplines` VALUES (9, 'Sky Raider');
INSERT INTO `ed_disciplines` VALUES (10, 'Swordmaster');
INSERT INTO `ed_disciplines` VALUES (11, 'Thief');
INSERT INTO `ed_disciplines` VALUES (12, 'Troubadour');
INSERT INTO `ed_disciplines` VALUES (13, 'Warrior');
INSERT INTO `ed_disciplines` VALUES (14, 'Weaponsmith');
INSERT INTO `ed_disciplines` VALUES (15, 'Wizard');
INSERT INTO `ed_disciplines` VALUES (16, 'Shaman');
INSERT INTO `ed_disciplines` VALUES (17, 'Horror Stalker');
INSERT INTO `ed_disciplines` VALUES (18, 'Boatman');
INSERT INTO `ed_disciplines` VALUES (19, 'Journeyman');
INSERT INTO `ed_disciplines` VALUES (20, 'Wind-Dancer');
INSERT INTO `ed_disciplines` VALUES (21, 'Windmaster');
INSERT INTO `ed_disciplines` VALUES (22, 'Windscout');
INSERT INTO `ed_disciplines` VALUES (23, 'Woodsman');
INSERT INTO `ed_disciplines` VALUES (24, 'Liberator');
INSERT INTO `ed_disciplines` VALUES (25, 'Outcast Warrior');
INSERT INTO `ed_disciplines` VALUES (26, 'Purifier');
INSERT INTO `ed_disciplines` VALUES (27, 'Traveled Scholar');
