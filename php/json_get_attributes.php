<?php

    require_once './database.php';

	header("Content-Type: application/json");

	echo "[";
		
	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);
	
	$query = "SELECT *
				FROM ed_attributes
				ORDER BY id;";
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		if(!$first) {
			echo ",";
		}
		$row['id'] = (int)$row['id'];
		echo json_encode($row);
		$first = false;
	}

	echo "]";
	
?>