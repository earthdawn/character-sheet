<?php

	require_once './database.php';

	header("Content-Type: application/json");

	echo "[";
	
	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);
	
	$restrictions = array();
	$query = "SELECT *
				FROM ed_racial_restrictions;";
	$result = $dbhandle->query($query);
	while($row = $result->fetch_assoc()) {
		$restrictions[(int)$row['race_id']][] = (int)$row['discipline_id'];
	}
	$result->free();
	
	$query = "SELECT *
				FROM ed_races
				ORDER BY name;";
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		if(!$first) {
			echo ",";
		}
		$row['id'] = (int)$row['id'];
		$row['restrictions'] = is_array($restrictions[$row['id']]) ? $restrictions[$row['id']] : array();
		echo json_encode($row);
		$first = false;
	}

	echo "]";
	
?>