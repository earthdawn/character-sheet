<?php

	require_once './database.php';

	header("Content-Type: application/json; charset=utf-8");

	/*
		Possible parameters are:
			discipline: numeric id	(required)
			circles[]: numeric array (optional)
			versatility: flag	(optional)
			
		Returns:
			{ core: [], support: [] }
			If "versatility" is defined then "core" contains all core talents of other disciplines and "support" all support talents
			that are not also available as core talents, else "core" and "support" contain only the talents of the given discipline.
			If "circles" are defined then only talents from that circles (core) or the corresponding status group (support) are returned.
	*/
	
	if(!isset($_GET['discipline']) || !is_numeric($_GET['discipline'])) {
		exit();
	}
	
	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);
	
	$discipline_id = $dbhandle->real_escape_string($_GET['discipline']);
	
	$circles = array();
	if(is_array($_GET['circles'])) {		
		foreach($_GET['circles'] as $circle) {
			$circles[] =  $dbhandle->real_escape_string($circle);
		}
	}
	
	$duplicates = array();	// use this instead of a "GROUP BY talent_id" clause because some talents (e.g. spell matrices) have the same id but should be regarded as different.
	$query = "";
	if(isset($_GET['versatility'])) {
		$query = "SELECT talent_id
					FROM ed_core_talents
					WHERE discipline_id = '{$discipline_id}';";
		$result = $dbhandle->query($query);
		while($row = $result->fetch_row()) {
			$duplicates[$row[0]] = true;
		}
		$result->free();
		
		$query = "SELECT talent_id
					FROM ed_support_talents
					WHERE discipline_id = '{$discipline_id}';";
		$result = $dbhandle->query($query);
		while($row = $result->fetch_row()) {
			$duplicates[$row[0]] = true;
		}
		$result->free();
	}
	
	// core talents
	echo "{\"core\":[";	
	
	if(isset($_GET['versatility'])) {
		$query = "SELECT core.*, tal.name, tal.karma, tal.action, tal.strain, tal.attribute, tal.stepbonus, grp.name AS status, grp.id AS status_id FROM
				ed_core_talents AS core JOIN ed_talents AS tal ON core.talent_id = tal.id
				JOIN ed_circles AS cir ON core.circle = cir.circle 
				JOIN ed_status_group AS grp ON cir.status_group_id = grp.id 
				WHERE core.discipline_id <> '{$discipline_id}'";
	} else {
		$query = "SELECT core.*, tal.name, tal.karma, tal.action, tal.strain, tal.attribute, tal.stepbonus, grp.name AS status, grp.id AS status_id FROM
				ed_core_talents AS core JOIN ed_talents AS tal ON core.talent_id = tal.id
				JOIN ed_circles AS cir ON core.circle = cir.circle
				JOIN ed_status_group AS grp ON cir.status_group_id = grp.id 
				WHERE core.discipline_id = '{$discipline_id}'";
	}
	if(count($circles) > 0) {
		$query .= " AND (";
		$first = true;
		foreach($circles as $circle) {
			if(!$first) {
				$query .= " OR ";
			}
			$query .= "core.circle = '{$circle}'";
			$first = false;
		}
		$query .= ")";
	}
	$query .= " ORDER BY core.circle, tal.name;";
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		$id = $row['talent_id'];
		// personalize for each discipline
		switch($id) {
			case 237: // Thread Weaving 
				$id += 5000 + $row['discipline_id'];
				break;
			case 209: // Spell Matrix
				$id += $row['circle'] * 10000 + $row['discipline_id'];
				break;
			case 254:	// Versatility
				if(isset($_GET['versatility'])) {
					continue 2;
				}
				$id += 2000;	// do this in order to distinguish it from the versatility talent received automatically by humans. Only Journeyman have versatility.
				break;
		}
		if(isset($_GET['versatility'])) {
			if($duplicates[$id]) {
				continue;
			}
			$duplicates[$id] = true;
		}
		if(!$first) {
			echo ",";
		}
		if($row['attribute'] == null) {
			$row['attribute'] = "null";
		}
		echo "{\"id\":{$id},\"name\":\"" . utf8_encode($row['name']). "\",\"circle\":{$row['circle']},";
		echo "\"discipline\":{$row['discipline_id']},\"disc_talent\":{$row['discipline_talent']},";
		echo "\"status\":\"{$row['status']}\",\"action\":{$row['action']},\"karma\":{$row['karma']},";
		echo "\"strain\":{$row['strain']},\"attribute\":{$row['attribute']},\"stepbonus\":{$row['stepbonus']}";
		echo "}";
		$first = false;
	}
	$result->free();
	echo "],";
	
	
	// support talents
	echo "\"support\":[";
	
	$extra_join = "";
	$extra_where = "";
	if(count($circles) > 0) {
		$extra_join = " JOIN ed_circles AS cir ON cir.status_group_id = sup.status_group_id ";
		$extra_where = " AND (";
		$first = true;
		foreach($circles as $circle) {
			if(!$first) {
				$query .= " OR ";
			}
			$extra_where .= "cir.circle = '{$circle}'";
			$first = false;
		}
		$extra_where .= ") ";
	}
	
	if(isset($_GET['versatility'])) {
		$query = "SELECT sup.*, tal.name, tal.karma, tal.action, tal.strain, tal.attribute, tal.stepbonus, grp.name AS status
					FROM ed_support_talents AS sup 
					JOIN ed_talents AS tal ON sup.talent_id = tal.id
					JOIN ed_status_group AS grp ON sup.status_group_id = grp.id {$extra_join}
					LEFT JOIN ed_core_talents AS cor ON cor.talent_id = sup.talent_id 
					WHERE cor.discipline_id IS NULL AND sup.discipline_id <> '{$discipline_id}' {$extra_where} 
					GROUP BY talent_id 
					ORDER BY tal.name, status_group_id;";
	} else {
		$query = "SELECT sup.*, tal.name, tal.karma, tal.action, tal.strain, tal.attribute, tal.stepbonus, grp.name AS status
					FROM ed_support_talents AS sup 
					JOIN ed_talents AS tal ON tal.id = sup.talent_id
					JOIN ed_status_group AS grp ON sup.status_group_id = grp.id {$extra_join}
					WHERE sup.discipline_id = '{$discipline_id}' {$extra_where}
					ORDER BY sup.status_group_id, tal.name;";
	}
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		for($i = 1; $i <= $row['count']; ++$i) {
			$id = $row['talent_id'];
			// personalize for each discipline and count. Assumes that at each status group only one type of matrix is available.
			switch($id) {
				case 209: // Spell Matrix
				case 97: // Enhanced Matrix
				case 16: // Armored Matrix
				case 196: // Shared Matrix
					$id += $row['status_group_id'] * 10000 + $i * 1500 + $row['discipline_id'];
					break;
			}
			if(isset($_GET['versatility'])) {
				if($duplicates[$id]) {
					continue;
				}
				$duplicates[$id] = true;
			}
			if(!$first) {
				echo ",";
			}
			if($row['attribute'] == null) {
				$row['attribute'] = "null";
			}
			echo "{\"id\":{$id},\"name\":\"" . utf8_encode($row['name']) . "\",";
			echo "\"discipline\":{$row['discipline_id']},";
			echo "\"status\":\"{$row['status']}\",\"action\":{$row['action']},\"karma\":{$row['karma']},";
			echo "\"strain\":{$row['strain']},\"attribute\":{$row['attribute']},\"stepbonus\":{$row['stepbonus']}";
			echo "}";
			$first = false;
		}
	}
	$result->free();
	
	echo "]}";
	
?>