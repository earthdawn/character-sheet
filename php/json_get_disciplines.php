<?php

	require_once './database.php';

	header("Content-Type: application/json");

	echo "[";
	
	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);
	
	$query = "SELECT *, dur.unconsciousness, dur.death
				FROM ed_disciplines
				JOIN ed_durability AS dur ON id = dur.discipline_id
				ORDER BY name;";
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		if(!$first) {
			echo ",";
		}
		$row['id'] = (int)$row['id'];
		$row['unconsciousness'] = (int)$row['unconsciousness'];
		$row['death'] = (int)$row['death'];
		echo json_encode($row);
		$first = false;
	}

	echo "]";
	
?>