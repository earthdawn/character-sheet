<?php

	require_once './database.php';


	header("Content-Type: text/xml");

	echo "<?xml version=\"1.0\" ?>\n";
	echo "<span>";

	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);

	if(!isset($_GET['talent']) || !is_numeric($_GET['talent'])) {
		exit();
	}
	
	$talent_id = $dbhandle->real_escape_string($_GET['talent']);
	
	// get core talents
	$query = "SELECT dis.name, cor.* 
				FROM ed_disciplines AS dis JOIN ed_core_talents AS cor ON cor.discipline_id = dis.id
				WHERE cor.talent_id = '{$talent_id}'
				ORDER BY dis.name, cor.circle;";
	
	$result = $dbhandle->query($query);
	while($row = $result->fetch_assoc()) {
	
		if($row['discipline_talent'] == 1) {
			echo "<div style=\"font-style:italic;white-space:nowrap;\">";
		} else {
			echo "<div style=\"white-space:nowrap;\">";
		}
		echo "{$row['name']} ({$row['circle']})";
		echo "</div>\n";
	};
	$result->free();
	
	
	// get support talents
	$query = "SELECT dis.name, sta.name AS status 
				FROM ed_support_talents AS sup JOIN ed_disciplines AS dis ON sup.discipline_id = dis.id
				JOIN ed_status_group AS sta ON sup.status_group_id = sta.id
				WHERE sup.talent_id = '{$talent_id}'
				ORDER BY dis.name, sup.status_group_id;";
				
	$result = $dbhandle->query($query);
	while($row = $result->fetch_assoc()) {
	
		echo "<div style=\"white-space:nowrap;\">";
		echo "{$row['name']} ({$row['status']})";
		echo "</div>\n";
	};
	$result->free();			
	
	$dbhandle->close();
	
	echo "</span>";
?>