<?php

	$check = strpos($_SERVER['HTTP_REFERER'], "://<your.domain.goes.here>/");
	if($check === false || $check > 5) {
		exit;
	}

	$filename = "";
	if(isset($_POST["filename"])) {
		$filename = strip_tags(trim(stripslashes($_POST["filename"])));
		$filename = str_replace(array(".", " "), array("", "_"), $filename);
	}
	if(strlen($filename) == 0) {
		$filename = "earthdawn_character";
	} elseif(strlen($filename) > 30) {
		$filename = substr($filename, 0, 30);
	}
	$filename .= ".ed";

	header("Cache-Control: no-cache");
	header("Content-Type: application/x-ed-character; charset=utf-8");
	header("Content-Disposition: attachment; filename={$filename}");
	
	echo stripslashes($_POST["content"]);
?>
