<?php

	require_once './database.php';

	header("Content-Type: application/json");

	echo "[";
	
	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);
	
	$query = "SELECT * FROM ed_skills ORDER BY type, name;";
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		if(!$first) {
			echo ",";
		}
		$row['id'] = (int)$row['id'];
		$row['action'] = (int)$row['action'];
		$row['strain'] = (int)$row['strain'];
		$row['attribute'] = (int)$row['attribute'];
		$row['type'] = (int)$row['type'];
		if($row['specialization'] != null) {
			$row['specialization'] = (int)$row['specialization'];
		} else {
			unset($row['specialization']);
		}
		echo json_encode($row);
		$first = false;
	}

	echo "]";
	
?>