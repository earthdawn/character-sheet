<?php

	require_once './database.php';

	header("Content-Type: application/json");

	echo "[";
	
	$dbhandle = connectToDatabase();
	$dbhandle->autocommit(TRUE);
	
	$query = "SELECT cir.circle, cir.status_group_id AS 'group', grp.name AS status
				FROM ed_circles AS cir
				JOIN ed_status_group AS grp ON cir.status_group_id = grp.id
				ORDER BY cir.circle;";
	
	$result = $dbhandle->query($query);
	$first = true;
	while($row = $result->fetch_assoc()) {
		if(!$first) {
			echo ",";
		}
		$row['circle'] = (int)$row['circle'];
		$row['group'] = (int)$row['group'];
		echo json_encode($row);
		$first = false;
	}

	echo "]";
	
?>