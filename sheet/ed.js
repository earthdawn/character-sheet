﻿var ed = {
	constants: {
		attributes: ["strength", "dexterity", "toughness", "perception", "willpower", "charisma"],
		skillType: [,"general", "knowledge", "artisan", "language"],
		stepDice: [, "D4-2", "D4-1", "D4", "D6", "D8", "D10", "D12", "2D6", "D8+D6", "D10+D6", "D10+D8", "2D10", "D12+D10", "D20+D4", "D20+D6", "D20+D8", "D20+D10", "D20+D12", "D20+2D6", "D20+D8+D6"],
		defenses: [,2,3,3,4,4,4,5,5,6,6,7,7,7,8,8,9,9,10,10,10,11,11,12,12,13,13,13,14,14,15],
		movement: {
			full: [,25,28,30,32,35,38,40,43,48,50,54,57,60,65,70,75,80,85,90,100,110,120,130,140,150,160,170,180,200,220],
			combat: [,13,14,15,16,18,19,20,22,24,25,27,29,30,33,35,38,40,43,45,50,55,60,65,70,75,80,85,90,100,110]
		},
		carry: [,10,15,20,25,30,35,40,50,60,70,80,90,105,125,145,165,200,230,270,315,360,430,500,580,675,790,920,1075,1200,1450],
		deathRating: [,19,20,22,23,24,26,27,28,30,31,32,34,35,36,38,39,40,42,43,44,46,47,48,50,51,52,54,55,56,58],
		unconRating: [,10,11,13,14,15,17,18,19,21,22,24,26,27,28,29,31,32,34,35,36,39,40,41,43,44,45,47,48,49,51],
		woundThreshold: [,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,13,14,14,15,15,15,16,16,17],
		recovery: [,0.5,0.5,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,5,5,5,5,5],
		mysticArmor: [,0,0,0,0,0,0,0,0,0,0,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7]
	},
	console: {
		debug: false ? console.debug.bind(console) : function(){}
	}
};


(function(){
	var baseUrl = window.location.protocol + "//www.behindthemirrors.de/php/ED/json_",
		request = new Request.JSON({
					method: "get",
					link: "cancel",
					async: false
				});

	ed.Data = {
		talents: [],
		versatility: [],
		getTalentCostModifier: function(circle) {	// returns the number of steps the costs are shifted upwards in the lp cost table
			return ((circle - 1) / 4).toInt();
		},
		getStatusForCircle: function(circle, asGroupId) {
			if(!this.circles) {
				request.addEvent("success", function(json) {
					this.circles = json;
					this.circles.unshift(0);
				}.bind(this));
				request.send({
					url: baseUrl + "get_circles.php"
				});
				request.removeEvents("success");
			}
			var info = this.circles[circle];
			return asGroupId ? info.group : info.status;
		},
		getMinCircleForStatus: function(status) {
			if(!this.circles) {
				this.getStatusForCircle(1);
			}
			var len, i, id;
			id = typeOf(status) === "number" ? status : false;
			for(i = 1, len = this.circles.length; i < len; ++i) {
				if((id && this.circles[i].group === id) || (!id && this.circles[i].status === status)) {
					return i;
				}
			}
			return 0;
		},
		getAttributeName: function(id, lowercase) {
			if(!this.attributesById) {
				request.addEvent("success", function(json) {
					this.attributesById = [];
					json.each(function(item) {
						this.attributesById[item.id] = item.name;
					}, this);
				}.bind(this));
				request.send({
					url: baseUrl + "get_attributes.php"
				});
				request.removeEvents("success");
			}
			var name = this.attributesById[id] || "";
			return lowercase ? name.toLowerCase() : name;
		},
		getDisciplines: function() {
			if(!this.disciplines) {
				request.addEvent("success", function(json) {
					this.disciplines = json;
					this.disciplinesById = [];
					this.disciplines.each(function(item) {
						this.disciplinesById[item.id] = item;
					}, this);
				}.bind(this));
				request.send({
					url: baseUrl + "get_disciplines.php"
				});
				request.removeEvents("success");				
			}
			return this.disciplines;
		},
		getDurability: function(discipline) {
			if(!this.disciplinesById) {
				this.getDisciplines();
			}
			var info = this.disciplinesById[discipline],
				durability = {
					unconsciousness: info.unconsciousness,
					death: info.death
				};
			return durability;
		},
		getRaces: function() {
			if(!this.races) {
				request.addEvent("success", function(json) {
					this.races = json;
				}.bind(this));
				request.send({
					url: baseUrl + "get_races.php"
				});
				request.removeEvents("success");
			}
			return this.races;
		},
		getTalents: function(discipline, maxCircle, minCircle) {
			minCircle = minCircle || 1;
			var result = {
					core: [],
					support: []
				},
				i, len, talents;
			this.getStatusForCircle(1);		// necessary to trigger request, else json is overwritten
			ed.console.debug("Data.getTalents d:" + discipline + " c:" + maxCircle);
			if(!this.talents[discipline]) {
				request.addEvent("success", function(json) {
					var talents = this.talents[discipline] = {
						core: [],
						support: []
					};
					json.core.each(function(item) {
						var list = talents.core[item.circle];
						if(!list) {
							talents.core[item.circle] = list = [];
						}
						list.push(item);
					});
					json.support.each(function(item) {
						item.minCircle = this.getMinCircleForStatus(item.status);					
						var list = talents.support[item.minCircle];
						if(!list) {
							talents.support[item.minCircle] = list = [];
						}
						list.push(item);
					}, this);
				}.bind(this));
				request.send({
					url: baseUrl + "get_talents.php?discipline=" + discipline
				});
				request.removeEvents("success");
			}
			talents = this.talents[discipline];
			for(i = minCircle, len = talents.core.length; i < len && i <= maxCircle; ++i) {
				if(talents.core[i]) {
					result.core.append(talents.core[i]);
				}
			}
			for(i = minCircle, len = talents.support.length; i < len && i <= maxCircle; ++i) {
				if(talents.support[i]) {
					result.support.append(talents.support[i]);
				}
			}
			return result;
		},
		getVersatilityTalents: function(discipline, maxCircle, minCircle) {
			minCircle = minCircle || 1;
			var result = [], talents;
			this.getStatusForCircle(1);		// necessary to trigger request, else json is overwritten
			this.getDisciplines();
			ed.console.debug("Data.getVersatilityTalents d:" + discipline + " c:" + maxCircle);
			if(!this.versatility[discipline]) {
				request.addEvent("success", function(json) {
					var talents = this.versatility[discipline] = [];
					json.core.each(function(item) {
						var list = talents[item.circle];
						if(!list) {
							talents[item.circle] = list = [];
						}
						item.versatility = true;
						item.name += " (" + this.disciplinesById[item.discipline].name + ")";
						list.push(item);
					}, this);
					json.support.each(function(item) {
						item.minCircle = this.getMinCircleForStatus(item.status);
						var list = talents[item.minCircle];
						if(!list) {
							talents[item.minCircle] = list = [];
						}
						item.versatility = true;
						item.name += " (" + this.disciplinesById[item.discipline].name + ")";
						list.push(item);
					}, this);
				}.bind(this));
				request.send({
					url: baseUrl + "get_talents.php?versatility=1&discipline=" + discipline
				});
				request.removeEvents("success");
			}
			talents = this.versatility[discipline];
			for(i = minCircle, len = talents.length; i < len && i <= maxCircle; ++i) {
				if(talents[i]) {
					result.append(talents[i]);
				}
			}
			return result;
		},
		getVersatility: function() {
			var talent = {
				id: 254,
				name: "Versatility",
				circle: 1,
				disc_talent: 0
			};
			return talent;
		},
		getSkills: function() {
			if(!this.skills) {
				request.addEvent("success", function(json) {
					this.skills = json;
				}.bind(this));
				request.send({
					url: baseUrl + "get_skills.php"
				});
				request.removeEvents("success");
			}
			return this.skills;
		}
	};
})();

ed.Generator = new Class({
	Implements: [Options, Events],
	options: {
		attributes: {
			points: 66,
			starting: {
				dexterity: 5,
				strength: 5,
				toughness: 5,
				perception: 5,
				willpower: 5,
				charisma: 5
			},
			min: 2,
			max: 18,
			increases: 5,
			costs: [,,-3,-2,-1,0,1,2,3,4,5,6,8,10,13,16,19,21,23]
		},
		talents: {
			freebies: 8,
			maxStartRank: 3,
			max: 15,
			minimum: [{ 
				name: "Karma Ritual",
				id: 135,
				rank: 1}],
			versatility: "Versatility",		// id:254 (and 2254 for journeymen)
			durability: 80		// id of the durability talent
		},
		skills: {
			max: 10,
			maxStartRank: 3,
			freebies: {
				attribute: "charisma",		// the attribute which's step should be used as general freebies
				artisan: 1,
				knowledge: 2,
				language: 2,
				general: 0
			},
			minimum: [{
						name: "Speak Language",
						id: 159,
						rank: 2 }]
		},
		lp: {
			starting: 0
		},
		discipline: {
			journeyman: 19		// id of the journeyman discipline
		},
		races: [
			null,	// dummy
			{	// 1:blood elf
				attributes: {
					modifier: {
						dexterity: 1,
						toughness: -2,
						willpower: 1,
						charisma: 3,
						social: 1,
						spell: 1,
						recovery: -1
					},
					minimum: {
						toughness: 3,
						willpower: 12
					}
				},
				abilities: ["Low-Light Vision", "Increased Social Defense", "Increased Spell Defense", "Pain Resistance", "Reduced Recovery Tests", "Armor Restrictions", "Wood Longing"],
				karma: {
					die: "D6",
					step: 4,
					cost: 10,
					starting: 5,
					maximum: 25
				},
				movement: {
					land: 1
				}
			},
			{	// 2:dwarf
				attributes: {
					modifier: {
						strength: 2,
						toughness: 3,
						charisma: -1
					},
					minimum: {
						charisma: 2
					}
				},
				abilities: ["Heat Sight (250y)"],
				karma: {
					step: 4,
					cost: 10,
					starting: 5,
					maximum: 25
				},
				movement: {
					land: -2
				}
			},
			{	// 3:elf
				attributes: {
					modifier: {
						dexterity: 2,
						toughness: -2,
						perception: 1,
						willpower: 1,
						charisma: 1
					},
					minimum: {
						toughness: 3
					}
				},
				abilities: ["Low-Light Vision"],
				karma: {
					die: "D6",
					step: 4,
					cost: 10,
					starting: 5,
					maximum: 25
				},
				movement: {
					land: 1
				}
			},
			{	// 4:human
				attributes: {},
				abilities: ["Versatility"],
				karma: {
					step: 5,
					cost: 6,
					starting: 10,
					maximum: 40
				},
				movement: {
					land: 0
				}
			},
			{	// 5:obsidiman
				attributes: {
					modifier: {
						dexterity: -2,
						strength: 6,
						toughness: 4,
						perception: -1,
						charisma: -1,
						armor: 3,
						wound: 3
					},
					minimum: {
						dexterity: 3,
						strength: 15,
						perception: 2,
						charisma: 2
					}
				},
				abilities: ["Increased Wound Threshold", "Natural Armor"],
				karma: {
					step: 3,
					cost: 10,
					starting: 5,
					maximum: 20
				},
				movement: {
					land: -3
				}
			},
			{	//6:ork
				attributes: {
					modifier: {
						dexterity: -1,
						strength: 3,
						toughness: 1,
						willpower: -1,
						charisma: -1
					},
					minimum: {
						dexterity: 2,
						willpower: 2,
						charisma: 2
					}
				},
				abilities: ["Low-Light Vision"],
				karma: {
					step: 5,
					cost: 7,
					starting: 10,
					maximum: 40
				},
				movement: {
					land: 1
				}
			},
			{	//7:troll
				attributes: {
					modifier: {
						strength: 4,
						toughness: 2,
						perception: -1,
						willpower: 1
					},
					minimum: {
						strength: 11,
						toughness: 11
					}
				},
				abilities: ["Heat Sight"],
				karma: {
					step: 3,
					cost: 10,
					starting: 5,
					maximum: 20
				},
				movement: {
					land: 2
				}
			},
			{	//8:t'skrang
				attributes: {
					modifier: {
						dexterity: 1,
						toughness: 1,
						charisma: 1
					}
				},
				abilities: ["Tail Attack"],
				karma: {
					step: 4,
					cost: 8,
					starting: 5,
					maximum: 25
				},
				movement: {
					land: 0
				}
			},
			{	//9:windling
				attributes: {
					modifier: {
						dexterity: 1,
						strength: -4,
						toughness: -3,
						perception: 1,
						charisma: 2,
						physical: 2
					},
					minimum: {
						strength: 5,
						toughness: 4
					},
					maximum: {
						strength: 11
					}
				},
				abilities: ["Astral-Sensitive Sight", "Flight", "Increased Physical Defense"],
				karma: {
					step: 6,
					cost: 5,
					starting: 15,
					maximum: 60
				},
				movement: {
					land: -8,
					air: 2
				}
			}
		]
	},
	initialize: function(options) {
		this.lastError = "";
		this.buildLPTable();
		ed.Data.getRaces().each(function(race) {
			var info = this.options.races[race.id.toInt()];
			if(info) {
				Object.append(info, race);
			}
		}, this);
		this.setOptions(options);
		this.attributes = {
			increases: {},
			values: {}
		};
		this.talents = {
			freebies: this.options.talents.freebies,
			core: [],
			support: [],
			versatility: []
		};
		this.skills = {
			list: [],
			freebies: {
				artisan: this.options.skills.freebies.artisan,
				knowledge: this.options.skills.freebies.knowledge,
				general: this.options.skills.freebies.general,
				language: this.options.skills.freebies.language
			}
		};
		this.circle = 0;
		this.lp = this.options.lp.starting;
		this.lpTotal = this.lp;
		
		this.suppressSkillFreebiesChanged = true;
		this.setRace(2);	// Dwarf
		this.setDiscipline(ed.Data.getDisciplines()[0].id);
		this.resetSkills();
		delete this.suppressSkillFreebiesChanged;
	},
	setRace: function(race) {
		var info = this.options.races[race],
			attribute, atts, increase, talents, i;
		if(!info || this.race === race) {
			return;
		}
		ed.console.debug("setRace: " + race);
		if(this.race) {
			// give back lp and freebies for talents bought through versatility
			this.talents.versatility.each(function(item) {
				if(item.bought) {
					this.setTalent(item.id, 0, true);
				}
			}, this);
			this.talents.versatility = [];
			if(!this.hasVersatility(race)) {
				// Remove Versatility talent if race is not human
				talents = this.talents.core;
				for(i = talents.length - 1; i >= 0; --i) {
					if(talents[i].name === this.options.talents.versatility) {
						if(talents[i].bought) {
							this.setTalentInternal(talents[i], 0 , true, talents[i].bought);
						}
						talents.splice(i, 1);
					}
				}
			}
			if(this.karma && this.karma > this.options.races[this.race].karma.starting) {
				i = this.options.races[this.race].karma;
				this.lp += (this.karma - i.starting) * i.cost;
			}
		}
		this.race = race;
		atts = this.attributes;
		for(attribute in atts.increases) {
			this.setAttribute(attribute, 0);
		}
		atts.increases = {};
		atts.points = this.options.attributes.points;
		for(attribute in this.options.attributes.starting) {
			atts.values[attribute] = this.options.attributes.starting[attribute];
			this.attributeChanged(attribute);
		}
		if(info.attributes.minimum) {
			for(attribute in info.attributes.minimum) {
				if(info.attributes.minimum[attribute] > atts.values[attribute]) {
					this.buyAttribute(attribute, info.attributes.minimum[attribute]);
				}
			}
		}
		if(this.discipline) {
			if(!this.checkDiscipline(this.discipline, this.race)) {
				ed.Data.getDisciplines().some(function(item) {
					if(this.checkDiscipline(item.id, this.race)) {
						this.setDiscipline(item.id);
						return true;
					}
					return false;
				}, this);
			} else if(this.hasVersatility()) {
				// add Versatility talent for Humans.
				// retrieve talents accessible through versatility.
				this.talents.core.push(Object.merge({rank: 0}, ed.Data.getVersatility()));
				talents = ed.Data.getVersatilityTalents(this.discipline, 15);
				talents.each(function(item) {
					this.talents.versatility.push(Object.merge({rank: 0}, item));
				}, this);
			}
		}
		this.karma = info.karma.starting;
		ed.console.debug("setRace end");
	},
	hasVersatility: function(race) {
		return this.options.races[race || this.race].abilities.contains("Versatility");
	},
	checkDiscipline: function(discipline, race) {
		var info = this.options.races[race];
		if(info) {
			return !info.restrictions || !info.restrictions.contains(discipline);
		}
		return false;
	},
	getAbilities: function() {
		return this.options.races[this.race].abilities;
	},
	setDiscipline: function(id) {
		if(id !== this.discipline) {
			ed.console.debug("setDiscipline " + id);
			if(!this.checkDiscipline(id, this.race)) {
				return this.error("Discipline " + id + " is restricted for " + this.options.races[this.race].name);
			}
			this.discipline = id;
			this.setCircle(0);
			this.setCircle(1);
			if(this.discipline === this.options.discipline.journeyman) {
				this.setTalent(254, 1, true);		// both versatility talents
				this.setTalent(2254, 1, true);
			} else if(this.options.talents.minimum) {
				this.options.talents.minimum.each(function(talent) {
					this.setTalent(talent.id, talent.rank, true);
				}, this);
			}
			ed.console.debug("setDiscipline end");
			return true;
		}
	},
	getLp: function(total) {
		return total ? this.lpTotal : this.lp;
	},
	setLp: function(available) {
		if(available < 0) {
			return this.error("Can't set available legend points to negative value");
		}
		var diff = available - this.lp;
		this.lp = available;
		this.lpTotal += diff;
	},
	getCircle: function() {
		return this.circle;
	},
	setCircle: function(circle) {
		var fn, talents, attribute;
		if(this.circle !== circle && circle <= 15) {
			ed.console.debug("setCircle " + circle);
			if(circle < this.circle) {
				ed.console.debug("  -decrease circle from " + this.circle);
				fn = function(item) {
					if(item.bought && item.bought > circle) {
						this.setTalentInternal(item, 0, true, item.bought);
					}
				};
				this.circle = circle;
				this.talents.versatility.each(fn, this);
				this.talents.core.each(fn, this);
				this.talents.support.each(fn, this);				
				for(attribute in this.attributes.increases) {
					this.setAttribute(attribute, 0);
				}
			} else if(circle > 0) {
				if(this.circle) {	// new circle is higher than current circle, check for advancement
					ed.console.debug("  -increase circle from " + this.circle);
					if(!this.checkCircleAdvancement(circle)) {
						return false;
					}
				} else {	// current circle is only 0 if the discipline changed, therefore reload all talents
					ed.console.debug("  -setup new circle");
					this.talents.core = [];
					this.talents.support = [];
					this.talents.versatility = [];
					talents = ed.Data.getTalents(this.discipline, 15);
					talents.core.each(function(item) {
						this.talents.core.push(Object.merge({rank: 0}, item));
					}, this);
					talents.support.each(function(item) {
						this.talents.support.push(Object.merge({rank: 0}, item));
					}, this);
					if(this.hasVersatility()) {
						this.talents.core.push(Object.merge({rank: 0}, ed.Data.getVersatility()));
						talents = ed.Data.getVersatilityTalents(this.discipline, 15);
						talents.each(function(item) {
							this.talents.versatility.push(Object.merge({rank: 0}, item));
						}, this);
					}
				}
			}
			this.circle = circle;
			ed.console.debug("setCircle end");
			return true;
		}
		return false;
	},
	checkCircleAdvancement: function(circle, replacement, newRank) {	// replacement and rank are optional
		var count = circle + 3,
			found = false, fn;
		ed.console.debug("checkCircleAdvancement");
		if(circle < 2) {
			return true;
		}
		fn = function(talent) {
			var value = (!replacement || talent.id !== replacement.id) ? talent.rank : newRank;
			if(value >= circle) {
				--count;
				if(talent.bought === circle - 1) {
					found = true;
				}
			}
			return found && count <= 0;
		};
		if(this.discipline === this.options.discipline.journeyman) {
			if(this.talents.versatility.some(fn)) {
				return true;
			}
		} else {
			if(this.talents.core.some(fn)) {
				return true;
			}
			if(this.talents.support.some(fn)) {
				return true;
			}
		}
		return this.error("Advancement to circle " + circle + " needs " + (circle + 3) + " talents with rank >=" + circle + " (short by: " + (count < 0 ? 0 : count) + "), one of which must come from the current circle (satisfied: " + (found ? "yes" : "no") + ").");
	},
	buyAttribute: function(attribute, value) {		// buy attributes with buying points
		var info = this.options.races[this.race],
			costs = this.options.attributes.costs,
			delta;
		ed.console.debug("buyAttribute " + attribute + " " + value);
		if(value < this.options.attributes.min) {
			return this.error("Attribute base value must not be less than " + this.options.attributes.min);
		}
		if(value > this.options.attributes.max) {
			return this.error("Attribute base value must not be greater than " + this.options.attributes.max);
		}
		if(info.attributes.minimum && info.attributes.minimum[attribute] && info.attributes.minimum[attribute] > value) {
			return this.error(attribute + " base value must not be less than " + info.attributes.minimum[attribute]);
		}
		if(info.attributes.maximum && info.attributes.maximum[attribute] && info.attributes.maximum[attribute] < value) {
			return this.error(attribute + " base value must not be greater than " + info.attributes.maximum[attribute]);
		}
		delta = costs[value] - costs[this.attributes.values[attribute]];
		if(delta > this.attributes.points) {
			return this.error("Not enough buying points left. Cost for new attribute value: " + delta);
		}
		this.attributes.points -= delta;
		this.attributes.values[attribute] = value;
		this.attributeChanged(attribute);
		return true;
	},
	setAttribute: function(attribute, value) {		// set attribute with lp
		var atts = this.attributes,
			count, key, found = false, costs;
		ed.console.debug("setAttribute " + attribute + " " + value);
		if(value < 0 || value > this.options.attributes.increases) {
			return this.error("A single attribute may only be increased upto " + this.options.attributes.increases + " times");
		}
		atts.increases[attribute] = atts.increases[attribute] || 0;
		if(atts.increases[attribute] < value) {
			count = 0;
			for(key in atts.increases) {
				if(key === attribute) {
					count += value;
					found = true;
				} else {
					count += atts.increases[key] || 0;
				}			
			}
			if(!found) {
				count += value;
			}
			if(count > this.circle - 1) {
				return this.error("No attribute increase possible. Each circle advancement grants only one attribute increase.");
			}
			costs = 0;
			count = atts.increases[attribute];
			while(count < value) {
				++count;
				costs += this.options.lp.costs[4 + count];
			}
			if(costs > this.lp) {
				return this.error("Not enough LP left for attribute increase. Needed: " + costs);
			}
			this.lp -= costs;
			atts.increases[attribute] = value;
		} else {
			while(atts.increases[attribute] > value) {
				this.lp += this.options.lp.costs[4 + atts.increases[attribute]];
				atts.increases[attribute]--;
			}
		}
		this.attributeChanged(attribute);
		return true;
	},
	attributeChanged: function(attribute) {
		var step, i, skill, count, 
			ranksLost = false;
		if(this.options.skills.freebies.attribute === attribute) {
			step = this.getAttributeStep(attribute);
			for(i = this.skills.list.length - 1; i >= 0; --i) {
				skill = this.skills.list[i];
				step -= skill.freebies.general;
				if(step < 0) {
					count = step + skill.getFreebiesTotal();
					step = 0;
					this.setSkillFreebieInternal(skill, count);
					if(skill.rank === 0) {
						this.skills.list.splice(i, 1);
					}
					ranksLost = true;
				}
			}
			this.skills.freebies.general = step;
			if(!this.suppressSkillFreebiesChanged) {
				this.fireEvent("skillFreebiesChanged", [ranksLost]);
			}
		}
	}.protect(),
	getAttributeIncreases: function(attribute) {
		return this.attributes.increases[attribute] || 0;
	},
	getAttribute: function(attribute, modified) {
		var value = this.attributes.values[attribute] || 0,
			info = this.options.races[this.race];
		if(modified) {
			if(info.attributes.modifier) {
				value += (info.attributes.modifier[attribute] || 0);
			}
			value += this.attributes.increases[attribute] || 0;
		}
		return value;
	},
	getAttributeStep: function(attribute) {
		var value = this.getAttribute(attribute, true);
		return 1 + Math.ceil(value / 3.0);
	},
	getTalents: function() {	// returns all non-zero talents only.
		var result = [],
			fn = function(talent) {
				if(talent.rank > 0) {
					result.push(talent);
				}
			};
		this.talents.core.each(fn);
		this.talents.support.each(fn);
		this.talents.versatility.each(fn);
		return result;
	},
	getAllTalents: function(circle) {	// returns all talents upto circle
		var result = {},
			list,
			fn = function(talent) {
				if((talent.circle && talent.circle <= circle) ||
					(talent.minCircle && talent.minCircle <= circle)) {
					list.push(talent);
				}
			};
		["core","support","versatility"].each(function(key) {
			list = [];
			this.talents[key].each(fn);
			result[key] = list;
		}, this);
		return result;
	},
	addTalent: function(id, circle) {	// adds a talent with rank 1 if it does not exist yet with a higher rank. tries to use freebies.
		var info = this.getTalentInternal(id, circle);
		if(!info) {
			return false;
		}
		if(info.talent.rank === 0) {
			return this.setTalentInternal(info.talent, 1, true, info.circle);
		}
		return false;
	},
	setTalent: function(id, value, freebie, circle) {
		var info = this.getTalentInternal(id, circle);
		if(!info) {
			return false;
		}
		return this.setTalentInternal(info.talent, value, freebie, info.circle);
	},
	setTalentInternal: function(talent, value, freebie, circle) {		// circle is the circle on which this talent should be bought (might be important for support talents). freebie may be: false (no freebies), "only" (use only freebies to buy/sell), true (use freebies first if available then lp).
		var delta, costs, costMod, i, freebieDelta;
		ed.console.debug("setTalentInternal " + talent.name + " " + value);
		if(value < 0) {
			return this.error("Talent rank must not be less than 0");
		}
		if(value > this.options.talents.max) {
			return this.error("Maximum possible talent rank is " + this.options.talents.max);
		}
		delta = value - talent.rank;
		if(delta === 0) {
			return false;
		}
		costMod = ed.Data.getTalentCostModifier(circle);
		if(delta > 0) {		// increase
			if(talent.rank === 0) {
				// check if support or versatility talent allowed (free slots available).
				if(talent.versatility && !this.hasFreeVersatility()) {
					return this.error("No free versatility slot left to buy talent. Increase your versatility rank first.");
				}
				if(talent.disc_talent == undefined && !talent.versatility) {
					circle = this.getSlotForSupportTalent(circle, talent.minCircle);
					if(circle === 0) {
						return this.error("No free support talent slot left for talent with minimum circle " + talent.minCircle);
					}
					costMod = ed.Data.getTalentCostModifier(circle);
				}
			}
			if(freebie === "only") {
				return this.setTalentFreebieInternal(talent, circle, value);
			}
			if(freebie && circle === 1 && talent.freebies < this.options.talents.maxStartRank) {
				freebieDelta = Math.min(Math.min(delta, this.options.talents.maxStartRank - talent.freebies), this.talents.freebies);
				if(!this.setTalentFreebieInternal(talent, circle, talent.freebies + freebieDelta)) {
					return false;
				}
			}
			costs = 0;
			for(i = talent.rank + 1; i <= value; ++i) {
				costs += this.options.lp.costs[i + costMod];
			}
			if(costs > this.lp) {
				return this.error("Not enought LP left (" + this.lp + "). Needed: " + costs);
			}
			this.lp -= costs;
		} else {	// decrease
			if(!freebie && talent.rank + delta < talent.freebies) {
				return this.error("Can't lower talent ranks bought with LP, only free ranks left.");
			}
			if(talent.name === this.options.talents.versatility && (this.hasFreeVersatility() + delta) < 0) {
				return this.error("Can't lower Versatility talent because its ranks are already used by foreign talents. Remove foreign-discipline talents first.");
			}			
			// check circle requirements
			if(!this.checkCircleAdvancement(this.circle, talent, value)) {
				return this.error("Can't lower talent to " + value + " because then the current circle requirements would not be met.");
			}
			
			if(freebie !== "only") {
				freebieDelta = talent.rank - talent.freebies + delta;
				if(freebieDelta < 0) {
					delta -= freebieDelta;
				} else {
					freebieDelta = 0;
				}
				costs = 0;
				for(i = delta + 1; i <= 0; ++i) {
					costs += this.options.lp.costs[talent.rank + i + costMod];
				}
				talent.rank += delta;
				this.lp += costs;
				delta = freebieDelta;
			} else if(delta + talent.freebies < 0) {
				return this.error("Could not remove " + (-delta) + " free rank(s), because only " + talent.freebies + " was/were invested into this talent");
			}
			if(freebie && delta < 0) {
				if(!this.setTalentFreebieInternal(talent, circle, talent.freebies + delta)) {
					return false;
				}
			}
		}
		talent.rank = value;
		talent.bought = value ? circle : 0;
		return true;
	}.protect(),
	setTalentFreebie: function(id, numFreebies) {
		var info = this.getTalentInternal(id, 1);
		if(!info || numFreebies < 0) {
			return false;
		}
		return this.setTalentFreebieInternal(info.talent, info.circle, numFreebies);
	},
	setTalentFreebieInternal: function(talent, circle, numFreebies) {
		var delta, costs, costMod, i, tmp;
		ed.console.debug("setTalentFreebieInternal " + talent.name + " " + numFreebies);
		if(circle !== 1) {
			return this.error("Free ranks may only be assigned to talents from the first circle");
		}
		if(numFreebies < 0) {
			return this.error("Talent rank must not be less than 0");
		}
		if(numFreebies > this.options.talents.max) {
			return this.error("Maximum possible talent rank is " + this.options.talents.max);
		}
		// calculate delta
		delta = numFreebies - talent.freebies;
		if(delta === 0) {
			return true;
		}
		if(this.talents.freebies < delta) {
			return this.error("Not enough free ranks left");
		} else if(talent.freebies + delta > this.options.talents.maxStartRank) {
			return this.error("Maximum of " + this.options.talents.maxStartRank + " free ranks per talent allowed");
		}		
		costMod = ed.Data.getTalentCostModifier(circle);
		if(delta > 0) {
			if(talent.rank === 0) {
				// check if support or versatility talent allowed (free slots available).
				if(talent.versatility && !this.hasFreeVersatility()) {
					return this.error("No free versatility slot left to buy talent. Increase your versatility rank first.");
				}
				if(talent.disc_talent == undefined && !talent.versatility) {
					circle = this.getSlotForSupportTalent(circle, talent.minCircle);
					if(circle === 0) {
						return this.error("No free support talent slot left for talent with minimum circle " + talent.minCircle);
					}
					costMod = ed.Data.getTalentCostModifier(circle);
				}
			}
			costs = 0;
			if(talent.freebies < talent.rank) {		// there were lp invested into this talent, calculate how much (excluding the ranks bought with freebies)
				tmp = 0;
				for(i = talent.freebies + 1; i <= talent.rank && i <= talent.freebies + delta; ++i) {
					costs += this.options.lp.costs[i + costMod];	// this was paid previously
					++tmp;
				}
				talent.rank -= tmp;
			}
			this.lp += costs;
			this.talents.freebies -= delta;
			talent.freebies += delta;
			talent.rank += delta;
		} else {
			if(talent.name === this.options.talents.versatility && (this.hasFreeVersatility() + delta) < 0) {
				return this.error("Can't lower Versatility talent because its ranks are already used by foreign talents. Remove foreign-discipline talents first.");
			}
			// check circle requirements
			if(!this.checkCircleAdvancement(this.circle, talent, talent.rank + delta)) {
				return this.error("Can't lower talent by " + (-delta) + " because then the current circle requirements would not be met.");
			}
			
			costs = 0;
			if(talent.freebies < talent.rank) {		// there were lp invested into this talent, calculate how much (excluding the ranks bought with freebies)
				for(i = talent.freebies + 1; i <= talent.rank; ++i) {
					costs += this.options.lp.costs[i + costMod];	// this was paid previously
				}
			}
			talent.freebies += delta;
			this.talents.freebies -= delta;
			talent.rank += delta;
			if(costs > 0) {		// calculate new costs and give the difference in lp back
				for(i = talent.freebies + 1; i <= talent.rank; ++i) {
					costs -= this.options.lp.costs[i + costMod];
				}
				this.lp += costs;
			}
		}
		talent.bought = talent.rank ? circle : 0;
		return true;
	}.protect(),
	getTalentInternal: function(id, circle) {
		var talent = this.hasTalent(id, true);
		if(!talent) {
			return this.error("Talent " + id + " is not accessible for this discipline");
		}
		circle = talent.bought || circle || this.circle;
		// buy core talents always at their fixed circle
		if(talent.disc_talent != undefined && !talent.versatility) {
			circle = talent.circle;
		}
		if(circle > this.circle) {
			return this.error("Can't buy talent for circle " + circle + ". Character is only circle " + this.circle);
		}
		// check for versatility and support talents
		if((talent.circle && talent.circle > circle) || 
			(talent.minCircle && talent.minCircle > circle)) {
			return this.error("Talent " + id + " is not accessible at circle " + circle);
		}
		talent.freebies = talent.freebies || 0;
		return { talent: talent, circle: circle };
	}.protect(),
	hasTalent: function(id, includeZero) {
		var result = false,
			fn = function(item) {
				if(item.id === id) {
					result = item;
					return true;
				}
				return false;
			};
		if(this.talents.core.some(fn)) {
			return (includeZero || result.rank) ? result : false;
		}
		if(this.talents.support.some(fn)) {
			return (includeZero || result.rank) ? result : false;
		}
		if(this.talents.versatility.some(fn)) {
			return (includeZero || result.rank) ? result : false;
		}
		return result;
	},
	hasFreeVersatility: function() {
		var count = 0,
			name = this.options.talents.versatility,
			result;
		this.talents.core.each(function(talent) {
			if(talent.name === name) {
				count += talent.rank;
			}
		}, this);
		this.talents.versatility.each(function(talent) {
			if(talent.rank > 0) {
				--count;
			}
		});
		return count;
	},
	getSlotForSupportTalent: function(circle, minCircle) {		// circle is the desired circle to buy the talent on, minCircle is the minimum circle of the support slot.
		var slots = [], i;
		if(minCircle > circle) {
			return 0;
		}
		for(i = minCircle; i <= circle; ++i) {
			slots[i] = true;
		}
		this.talents.support.each(function(talent) {
			if(talent.rank && talent.bought >= minCircle && talent.bought <= circle) {
				slots[talent.bought] = false;
			}
		});
		for(i = minCircle; i <= circle; ++i) {
			if(slots[i]) {
				return i;
			}
		}
		return 0;
	},
	getSkillsById: function() {
		var result = [];
		this.skills.list.each(function(skill) {
			result[skill.id] = skill;
		});
		return result;
	},
	getSkillInternal: function(id) {
		var skill = null,
			fn = function(item) {
				if(item.id === id) {
					skill = item;
					return true;
				}
				return false;
			};
		if(this.skills.list.some(fn)) {
			return skill;
		}
		if(ed.Data.getSkills().some(fn)) {
			skill = Object.merge({}, skill, {rank: 0, freebies: { artisan: 0, knowledge: 0, general: 0, language: 0 }});
			skill.getFreebiesTotal = function() {
				var count = 0, key;
				for(key in this.freebies) {
					count += this.freebies[key];
				}
				return count;
			};
			return skill;
		}
		return this.error("Could not find skill with id " + id);
	}.protect(),
	addSkill: function(id) {
		var skill = this.getSkillInternal(id),
			add;
		if(!skill) {
			return null;
		}
		if(skill.rank > 0) {
			return skill;
		}
		this.suppressError = true;
		add = this.setSkillFreebieInternal(skill, 1);
		delete this.suppressError;
		if(!add) {
			add = this.setSkillInternal(skill, 1);
		}
		if(add && skill.rank > 0) {
			this.skills.list.include(skill);
			return skill;
		}
		return null;
	},
	setSkillFreebie: function(id, numFreebies) {
		var skill = this.getSkillInternal(id),
			add, result;
		if(!skill) {
			return false;
		}
		add = skill.rank === 0;
		result = this.setSkillFreebieInternal(skill, numFreebies);
		if(add) {
			if(skill.rank > 0) {
				this.skills.list.include(skill);
			}
		} else if(skill.rank === 0) {
			this.skills.list.erase(skill);
		}
		return result;
	},
	setSkillFreebieInternal: function(skill, numFreebies) {
		if(numFreebies < 0) {
			return this.error("Skill rank must not be less than 0");
		}
		if(numFreebies > this.options.skills.max) {
			return this.error("Maximum possible skill rank is " + this.options.skills.max);
		}
		if(numFreebies > this.options.skills.maxStartRank) {
			return this.error("Maximum starting skill rank reachable with free ranks is " + this.options.skills.maxStartRank);
		}
		var typeKey = ed.constants.skillType[skill.type],
			fn = function(skill, delta, type) {
					var costs, lpRanks, baseRank, i, tmp,
						skillCosts = this.options.lp.costs;
						
					lpRanks = skill.rank - skill.getFreebiesTotal();
					if(delta > 0) {
						if(this.skills.freebies[type] < delta) {
							tmp = delta - this.skills.freebies[type];
							if(type === "general" || this.skills.freebies.general < tmp - skill.freebies.general) {
								return this.error("Not enough free ranks left");
							}
							delta = this.skills.freebies[type];
						}
						if(lpRanks > 0) {	// there were lp invested into this skill, calculate how much (excluding the ranks bought with freebies and boni)
							costs = tmp = 0;
							baseRank = skill.rank - lpRanks;
							for(i = 1; i + baseRank <= skill.rank && i <= delta; ++i) {
								costs += skillCosts[i + baseRank];
								++tmp;
							}
							skill.rank -= tmp;
							this.lp += costs;
						}
						this.skills.freebies[type] -= delta;
						skill.freebies[type] += delta;
						skill.rank += delta;
					} else if(delta < 0) {
						if(skill.freebies[type] + delta < 0) {
							delta = -skill.freebies[type];
						}
						if(lpRanks > 0) {	// there were lp invested into this skill, calculate how much (excluding the ranks bought with freebies and boni) and credit the difference
							costs = 0;
							for(i = 0; i < lpRanks && i + delta < 0; ++i) {
								costs += skillCosts[skill.rank - i];
							}
							baseRank = skill.rank - lpRanks + delta;
							for(i = 1; i <= lpRanks && i + delta <= 0; ++i) {
								costs -= skillCosts[baseRank + i];
							}
							this.lp += costs;
						}
						skill.freebies[type] += delta;
						skill.rank += delta;
						this.skills.freebies[type] -= delta;
					}
					return true;
				}.bind(this),
			delta = numFreebies - skill.freebies.general;
		// calculate delta and set freebies
		if(typeKey !== "general") {
			if(!fn(skill, numFreebies - skill.freebies[typeKey], typeKey)) {
				return false;
			}
			delta -= skill.freebies[typeKey];
		}
		return fn(skill, delta, "general");
	}.protect(),
	setSkill: function(id, value) {
		var skill = this.getSkillInternal(id),
			add, result;
		if(!skill) {
			return false;
		}
		add = skill.rank === 0;
		result = this.setSkillInternal(skill, value);
		if(add) {
			if(skill.rank > 0) {
				this.skills.list.include(skill);
			}
		} else if(skill.rank === 0) {
			this.skills.list.erase(skill);
		}
		return result;
	},
	setSkillInternal: function(skill, value) {
		var delta, costs, i, lpRanks, freebieDelta,
			skillCosts = this.options.lp.costs;
		if(value < 0) {
			return this.error("Skill rank must not be less than 0");
		}
		if(value > this.options.skills.max) {
			return this.error("Maximum possible skill rank is " + this.options.skills.max);
		}
		delta = value - skill.rank;
		if(delta === 0) {
			return false;
		}
		if(delta > 0) {		// increase
			costs = 0;
			for(i = skill.rank + 1; i <= value; ++i) {
				costs += skillCosts[i];
			}
			if(costs > this.lp) {
				return this.error("Not enought LP left (" + this.lp + "). Needed: " + costs);
			}
			this.lp -= costs;
		} else {	// decrease
			lpRanks = skill.rank - skill.getFreebiesTotal();
			if(lpRanks + delta < 0) {
				if(!this.setSkillFreebieInternal(skill, skill.rank + delta)) {
					return false;
				}
				delta = -lpRanks;
			}
			costs = 0;
			for(i = delta + 1; i <= 0; ++i) {
				costs += skillCosts[skill.rank + i];
			}
			this.lp += costs;
		}
		skill.rank = value;
		return true;
	}.protect(),
	resetSkills: function() {
		var i;
		for(i = this.skills.list.length - 1; i >= 0; --i) {
			this.setSkillInternal(this.skills.list[i], 0);
		}
		this.skills.list = [];
		this.options.skills.minimum.each(function(item) {
			this.setSkillFreebie(item.id, item.rank);
		}, this);
	}.protect(),
	getKarma: function() {
		var karma = this.options.races[this.race].karma,
			result = {};
		Object.append(result, karma);
		result.available = this.karma;
		result.dice = this.getStepDice(result.step);
		return result;
	},
	buyKarma: function(points) {
		var info = this.options.races[this.race].karma,
			costs;
		if(points <= 0) {
			return;
		}
		if(this.karma + points > info.maximum) {
			points = info.maximum - this.karma;
			this.error("Can buy only " + points + " karma points. Racial maximum reached.")
		}
		if(points > 0) {
			costs = points * info.cost;
			if(costs > this.lp) {
				points = Math.floor(this.lp / info.cost);
				costs = points * info.cost;
				this.error("Not enough legend points available. Buying only " + points + " karma for " + costs + " LP.");
			}
			this.lp -= costs;
			this.karma += points;
		}
	},
	burnKarma: function(points) {
		if(points < 0) {
			return this.error("Can burn a negative amount of karma points.");
		}
		if(points > this.karma) {
			points = this.karma;
		}
		this.karma -= points;
	},
	getCharacteristics: function() {
		var toughness = this.getAttribute("toughness", true),
			dexterity = this.getAttribute("dexterity", true),
			info = this.options.races[this.race],
			result = {
				defenses: {
					physical: ed.constants.defenses[dexterity],
					spell: ed.constants.defenses[this.getAttribute("perception", true)],
					social: ed.constants.defenses[this.getAttribute("charisma", true)]
				},
				movement: {},
				carry: ed.constants.carry[this.getAttribute("strength", true)],
				death: {
					"base": ed.constants.deathRating[toughness]
				},
				unconsciousness: {
					"base": ed.constants.unconRating[toughness]
				},
				wound: ed.constants.woundThreshold[toughness],
				recovery: {
					tests: ed.constants.recovery[toughness],
					step: this.getAttributeStep("toughness")
				},
				armor: {
					physical: 0,
					mystic: ed.constants.mysticArmor[this.getAttribute("willpower", true)]
				}
			},
			key, modifer, tmp;
		result.recovery.dice = this.getStepDice(result.recovery.step);
		result.lift = result.carry * 2;
		tmp = this.hasTalent(this.options.talents.durability, false);
		if(tmp) {
			modifier = ed.Data.getDurability(this.discipline);
			result.unconsciousness.durability = tmp.rank * modifier.unconsciousness;
			result.death.durability = tmp.rank * modifier.death;
		}
		for(key in info.movement) {
			tmp = dexterity + info.movement[key];
			result.movement[key] = {
				full: ed.constants.movement.full[tmp < 3 ? 3 : tmp],
				combat: ed.constants.movement.combat[tmp < 3 ? 3 : tmp]
			};
		}
		modifier = info.attributes.modifier;
		if(modifier) {
			for(key in result.defenses) {
				result.defenses[key] += (modifier[key] || 0);
			}
			if(modifier.armor) {
				result.armor.physical += modifier.armor;
			}
			if(modifier.wound) {
				result.wound += modifier.wound;
			}
			if(modifier.recovery) {
				tmp = result.recovery.tests + modifier.recovery;
				result.recovery.tests = tmp > 0 ? tmp : 0;
			}
		}
		return result;
	},
	getStepDice: function(step) {
		return ed.constants.stepDice[step] || "";
	},
	save: function() {
		var state = {
				race: this.race,
				discipline: this.discipline,
				circle: this.circle,
				lp: this.lp,
				lpTotal: this.lpTotal,
				karma: this.karma,
				attributes: this.attributes,
				talents: {
					free: this.talents.freebies,
					core: {},
					support: {}
				},
				skills: {
					free: this.skills.freebies,
					list: []
				}
			},
			saveTalent = function(source, target) {
				var i, talent;
				for(i = source.length - 1; i >= 0; --i) {
					talent = source[i];
					if(talent.rank > 0) {
						target[talent.id] = [talent.rank, talent.bought, talent.freebies];
					}
				}
			},
			saveSkills = function(target) {
				var i, skill, saved;
				for(i = this.skills.list.length - 1; i >= 0; --i) {
					skill = this.skills.list[i];
					saved = [skill.id, skill.rank, skill.freebies.general, skill.type, skill.freebies[ed.constants.skillType[skill.type]]];
					target.push(saved);
				}
			}.bind(this);
		saveTalent(this.talents.core, state.talents.core);
		saveTalent(this.talents.support, state.talents.support);
		if(this.talents.versatility.length > 0) {
			state.talents.versatility = {};
			saveTalent(this.talents.versatility, state.talents.versatility);
		}
		saveSkills(state.skills.list);
		return state;
	},
	load: function(state) {
		if(!state) {
			return this.error("Load failed. No state information.");
		}
		var loadTalent = function(source, target) {
				var i, talent, saved;
				for(i = target.length - 1; i >= 0; --i) {
					talent = target[i];
					saved = source[talent.id];
					if(saved && saved.length === 3) {
						talent.rank = saved[0];
						talent.bought = saved[1];
						talent.freebies = saved[2];
					} else if(talent.rank > 0) {
						talent.rank = 0;
						talent.freebies = 0;
						talent.bought = 0;
					}
				}
			},
			loadSkills = function(source) {
				var i, skill, saved;
				this.skills.list = [];
				for(i = source.length - 1; i >= 0; --i) {
					saved = source[i];
					skill = this.getSkillInternal(saved[0]);
					if(skill) {
						skill.rank = saved[1];
						skill.freebies.general = saved[2];
						skill.freebies[ed.constants.skillType[saved[3]]] = saved[4];
						this.skills.list.push(skill);
					}
				}
			}.bind(this);
			
		this.suppressSkillFreebiesChanged = true;
		if(!state.skills) {
			this.resetSkills();
		}
		this.setRace(state.race);
		this.setDiscipline(state.discipline);
		this.circle = state.circle;
		this.lp = state.lp;
		this.lpTotal = state.lpTotal;
		this.karma = state.karma || this.karma;
		this.attributes = state.attributes;
		this.talents.freebies = state.talents.free;
		loadTalent(state.talents.core, this.talents.core);
		loadTalent(state.talents.support, this.talents.support);
		loadTalent(state.talents.versatility, this.talents.versatility || {});
		if(state.skills) {
			this.skills.freebies = state.skills.free;
			loadSkills(state.skills.list);
		} else if(this.options.skills.freebies.attribute) {
			this.attributeChanged(this.options.skills.freebies.attribute);
		}
		delete this.suppressSkillFreebiesChanged;
		return true;
	},
	reset: function() {
		var discipline = this.discipline,
			allDisc = ed.Data.getDisciplines();
		if(this.race === 2) {
			this.setRace(3);
		}
		this.setRace(2);
		if(discipline === this.discipline) {
			allDisc.some(function(item) {
				if(item.id !== discipline && this.checkDiscipline(item.id, this.race)) {
					this.setDiscipline(item.id);
					return true;
				}
				return false;
			}.bind(this));
		}
		this.setDiscipline(allDisc[0].id);
		this.resetSkills();
		this.lp = this.options.lp.starting;
		this.lpTotal = this.lp;
	},
	error: function(msg) {
		if(!this.suppressError) {
			this.lastError = msg;
			this.fireEvent("error", msg);
		}
		return false;
	},
	buildLPTable: function() {
		var table = [],
			fibonacci = function(val) {
				if(val == 0) {
					return 1;
				} else if (val < 0) {
					return 0;
				} else if (table[val]){
					return table[val];
				} else {
					var result = fibonacci(val - 1) + fibonacci(val - 2);
					table[val] = result;
					return result;
				}
			};
		fibonacci(20);
		this.options.lp.costs = table.map(function(item) {
			return item ? item * 100 : 0;
		});
	}.protect()
});


ed.gui = {};

ed.gui.UpDown = new Class({
	Implements: [Options, Events],
	options: {},
	initialize: function(field, options) {
		this.field = $(field);
		if(options.data) {
			this.data = options.data;
			delete options.data;
		}
		this.setOptions(options);
		this.container = new Element("div", {
			styles: {
				"float": "left",
				height: 21,
				width: 10,
				"font-weight": "bold",
				"line-height": "9px",
				"font-size": "12px",
				"margin": "0 2px 0 2px"
			},
			"class": "screen"
		});
		this.up = new Element("div", {
			styles: {
				height: 10,
				width: 8,
				"padding-left": "1px",
				"margin-bottom": "1px",
				border: "solid 1px #000000",
				cursor: "pointer",
				"background-color": "#404040",
				color: "#ffffff"
			},
			html: "+",
			events: {
				click: this.add.bind(this)
			}
		}).inject(this.container);
		this.down = new Element("div", {
			styles: {
				height: 10,
				width: 7,
				"line-height": "5px",
				"padding-left": "2px",
				border: "solid 1px #000000",
				cursor: "pointer",
				"background-color": "#404040",
				color: "#ffffff"
			},
			html: "-",
			events: {
				click: this.subtract.bind(this)
			}
		}).inject(this.container);
		this.container.inject(this.field, "after");
	},
	add: function(e) {
		e.stop();
		var value = this.getValue();
		this.setValue(value + 1);
		this.fireEvent("add", [this, value + 1, value]);
		return false;
	},
	subtract: function(e) {
		e.stop();
		var value = this.getValue();
		this.setValue(value - 1);
		this.fireEvent("subtract", [this, value - 1, value]);
		return false;
	},
	getValue: function() {
		if(this.field.value != null) {
			return this.field.value.toInt();
		}
		return this.field.get("text").toInt();
	},
	setValue: function(val) {
		if(this.field.value != null) {
			this.field.value = val;
		} else {
			this.field.set("text", val);
		}
		return this;
	},
	dispose: function() {
		this.field = null;
		delete this.data;
		this.up = this.down = null;
		this.container = this.container.destroy();
		this.removeEvents();
		return null;
	}
});


ed.gui.Toggles = new Class({
	Implements: [Options, Events],
	options: {
		count: 3
	},
	initialize: function(field, options) {
		this.field = $(field);
		if(options.data) {
			this.data = options.data;
			delete options.data;
		}
		this.setOptions(options);
		this.container = new Element("div", {
			styles: {
				"float": "left",
				height: 21,
				width: 6,
				"margin": "0 2px 0 2px"
			},
			"class": "screen"
		});
		this.value = 0;
		this.toggles = [];
		var i, elem;
		for(i = this.options.count - 1; i >= 0; --i) {
			elem = new Element("div", {
				styles: {
					height: "6px",
					width: "6px",
					"margin-bottom": "1px",
					"-moz-border-radius" : "5px",
					"-webkit-border-radius": "5px",
					border: "solid 1px #000000",
					"border-radius" : "5px",
					"background-color": "#ffffff",
					cursor: "pointer"
				},
				html: "&nbsp;",
				events: {
					click: this.clicked.bind(this, i)
				}
			}).inject(this.container);
			this.toggles[i] = {
				element: elem,
				state: false
			};
		}
		this.container.inject(this.field, "after");
	},
	clicked: function(index) {
		var state = !this.toggles[index].state,
			oldValue = this.value;
		this.setValue(state ? (index + 1) : index);
		this.fireEvent("change", [this, this.value, oldValue]);
		return false;
	},
	getValue: function() {
		return this.value;
	},
	setValue: function(val) {
		if(val < 0 || val > this.options.count) {
			return this;
		}
		this.toggles.each(function(toggle, index) {
			toggle.state = val > index;
			toggle.element.setStyle("background-color", (toggle.state ? "#00ff00" : "#ffffff"));
		});
		this.value = val;
		return this;
	},
	dispose: function() {
		this.field = null;
		delete this.data;
		this.toggles = null;
		this.container = this.container.destroy();
		this.removeEvents();
		return null;
	}
});


ed.gui.Message = new Class({
	Implements: [Options],
	options: {
		delay: 3000,
		hide: false,
		subkeys: []
	},
	initialize: function(container, options) {
		this.container = $(container);
		if(this.container.getStyle("display") === "none") {
			this.options.hide = true;
		}
		this.setOptions(options);
		this.sub = {};
		this.options.subkeys.each(function(key) {
			this.sub[key] = new Element("div").inject(this.container);
		}, this);
	},
	write: function(msg, key) {
		if(this.timer) {
			this.timer = clearTimeout(this.timer);
		}
		var elem = key ? this.sub[key] : this.container;
		elem.set("text", msg);
		if(this.options.hide) {
			elem.setStyle("display", "block");
		}
	},
	notice: function(msg, key) {
		var delay = this.options.delay + (msg.length * 20);
		this.write(msg, key);
		this.timer = this.clear.delay(delay, this, [key]);
	},
	clear: function(key) {
		var elem = key ? this.sub[key] : this.container;
		elem.set("text", "");
		if(this.options.hide) {
			elem.setStyle("display", "none");
		}
	}
});

ed.gui.Sheet = new Class({
	Implements: [Options, Events],
	options: {
		error: "error",
		info: "info",
		races: "races",
		disciplines: "disciplines",
		talents: "talents",
		skills: "skills",
		circle: "circle",
		abilities: "abilities",
		name: "name",
		magic: 43,
		//onLpChanged: $empty
	},
	initialize: function(options) {
		this.setOptions(options);
		
		this.noticeError = this.noticeError.bind(this);
		this.buyAttribute = this.buyAttribute.bind(this);
		this.lpAttribute = this.lpAttribute.bind(this);
		this.setTalent = this.setTalent.bind(this);
		this.setTalentFreebie = this.setTalentFreebie.bind(this);
		this.setSkill = this.setSkill.bind(this);
		this.setSkillFreebie = this.setSkillFreebie.bind(this);
		this.setCircle = this.setCircle.bind(this);
		this.skillFreebiesChanged = this.skillFreebiesChanged.bind(this);
		this.buyKarma = this.buyKarma.bind(this);
		this.burnKarma = this.burnKarma.bind(this);
		
		this.error = new ed.gui.Message(this.options.error);
		this.info = new ed.gui.Message(this.options.info, {subkeys: ["attributes", "talents", "skill-general", "skill-knowledge", "skill-artisan", "skill-language", "lp"]});
		
		var lp = /[?&]lp=(\d+)/.exec(window.location.search),
			genOptions = {
				onError: this.noticeError,
				onSkillFreebiesChanged: this.skillFreebiesChanged
			};
		if(lp && lp[1] > 0) {
			genOptions.lp = {
				starting: lp[1].toInt()
			};
		}
		
		this.generator = new ed.Generator(genOptions);		
		this.name = $(this.options.name);
		this.races = $(this.options.races);
		this.disciplines = $(this.options.disciplines);
		this.abilities = $(this.options.abilities);
		this.talents = $(this.options.talents);
		this.skills = $(this.options.skills);
		this.attributes = {};
		this.karma = {
			step: $("karma_step"),
			dice: $("karma_dice"),
			maximum: $("karma_maximum"),
			available: $("karma_available"),
			cost: $("karma_cost")
		};		
		this.setup();
	},
	setup: function() {
		ed.Data.getRaces().each(function(item) {
			this.races.options[this.races.options.length] = new Option(item.name, item.id);
		}, this);
		this.races.addEvent("change", function(e) {
			this.suppressRedraw = true;
			this.generator.setRace(e.target.value.toInt());
			this.fillDisciplines();
			delete this.suppressRedraw;
			this.redraw();
			return true;
		}.bind(this));
		this.disciplines.addEvent("change", function(e) {
			this.generator.setDiscipline(e.target.value.toInt());
			this.redraw();
			return true;
		}.bind(this));
		this.circle = new ed.gui.UpDown(this.options.circle, {
			onAdd: this.setCircle,
			onSubtract: this.setCircle
		});
		ed.constants.attributes.each(function(key) {
			var attribute = this.attributes[key] = {};
			attribute.value = new ed.gui.UpDown(key, {
				onAdd: this.buyAttribute,
				onSubtract: this.buyAttribute
			});
			attribute.increase = new ed.gui.UpDown(key + "_increase", {
				onAdd: this.lpAttribute,
				onSubtract: this.lpAttribute
			});
			attribute.modified = $(key + "_modified");
			attribute.step = $(key + "_step");
			attribute.dice = $(key + "_dice");
		}, this);
		$("karma_buy").addEvent("click", this.buyKarma);
		$("karma_burn").addEvent("click", this.burnKarma);
		
		this.addEvent("lpChanged", function(lp, total){ this.info.write("Available Legend points: " + lp, "lp"); }.bind(this), true);
		
		this.redraw();
	},
	fillDisciplines: function() {
		this.disciplines.options.length = 0;
		ed.Data.getDisciplines().each(function(item) {
			if(this.generator.checkDiscipline(item.id, this.generator.race)) {
				this.disciplines.options[this.disciplines.options.length] = new Option(item.name, item.id);
			}
		}, this);
	},
	getGenerator: function() {
		return this.generator;
	},
	setCircle: function(updown, value, oldValue) {
		if(value < 1 || !this.generator.setCircle(value)) {
			updown.setValue(oldValue);
		} else {
			this.redraw();
		}
	},
	buyAttribute: function(updown, value, oldValue) {
		var key = updown.field.id;
		if(value < 0 || !this.generator.buyAttribute(key, value)) {
			updown.setValue(oldValue);
		} else {
			this.info.write("Attribute buying points: " + this.generator.attributes.points, "attributes");
			this.drawAttribute(key);
			this.drawTalentsByAttribute(key);
			this.drawSkillsByAttribute(key);
			this.drawCharacteristics();
		}
	},
	lpAttribute: function(updown, value, oldValue) {
		var key = updown.field.id.slice(0, -9);
		if(value < 0 || !this.generator.setAttribute(key, value)) {
			updown.setValue(oldValue);
		} else {
			this.lpChanged();
			this.drawAttribute(key);
			this.drawTalentsByAttribute(key);
			this.drawSkillsByAttribute(key);
			this.drawCharacteristics();
		}
	},
	drawAttribute: function(key) {
		var attribute = this.attributes[key],
			step = this.generator.getAttributeStep(key);
		attribute.modified.set("text", this.generator.getAttribute(key, true));
		attribute.step.set("text", step);
		attribute.dice.set("text", this.generator.getStepDice(step));		
	},
	drawCharacteristics: function() {
		var values = this.generator.getCharacteristics(),
			move = function(obj, desc, keep) {
				var k, target, html;
				for(k in obj) {
					target = $(k + "_movement");
					html = desc ? "<div style=\"font-size:12px;white-space:nowrap;\">" : "<div>";
					html += (desc || "") + obj[k] + "y</div>";
					target.set("html", (keep ? target.get("html") : "") + html);
				}
			}, 
			key, i;
		$("initiative").set("text", this.generator.getAttributeStep("dexterity"));
		if(values.movement.air) {
			move(values.movement.land, "Land: ");
			move(values.movement.air, "Flight: ", true);
		} else {
			move(values.movement.land);
		}
		$("carry").set("text", values.carry);
		$("lift").set("text", values.lift);
		for(key in values.defenses) {
			$(key + "_defense").set("text", values.defenses[key]);
		}
		for(key in values.armor) {
			$(key + "_armor").set("text", values.armor[key]);
		}
		$("wound_threshold").set("text", values.wound);
		for(i = 5; i > 0; --i) {
			$("recovery_test" + i).setStyle("display", (values.recovery.tests >= i ? "" : "none"));
		}
		$("recovery_step").set("text", values.recovery.step);
		$("recovery_dice").set("text", values.recovery.dice);
		$("unconsciousness_base").set("text", values.unconsciousness.base);
		$("unconsciousness_durability").set("text", values.unconsciousness.durability || 0);
		$("unconsciousness").set("text", values.unconsciousness.base + (values.unconsciousness.durability || 0));
		$("death_base").set("text", values.death.base);
		$("death_durability").set("text", values.death.durability || 0);
		$("death").set("text", values.death.base + (values.death.durability || 0));
	},
	buyKarma: function() {
		var points = window.prompt("How many karma points should be bought with legend points? \nNote: If the race of the character is changed afterwards, the karma points will be reset.", "0");
		if(!points) {
			return false;
		}
		points = points.toInt();
		if(typeOf(points) == "number" && points > 0) {
			this.generator.buyKarma(points);
			this.drawKarma();
			this.lpChanged();
		}
		return false;
	},
	burnKarma: function() {
		var points = window.prompt("How many karma points should be burned? \nThese points are lost forever. No legend points are refunded for this karma decrease.", "0");
		if(!points) {
			return false;
		}
		points = points.toInt();
		if(typeOf(points) == "number" && points > 0) {
			this.generator.burnKarma(points);
			this.drawKarma();
		}
		return false;
	},
	buildTalent: function(talent, option, sorted) {
		var info = {
				container: new Element("div", {
					"class": "talent"
				}),
				option: option,
				talent: talent,
				dispose: function() {
					this.option = this.option.activate(true);
					this.talent = null;
					this.rank = null;
					this.updown = this.updown.dispose();
					this.toggles = this.toggles.dispose();
					this.container.eliminate("talent");
					this.container = this.container.destroy();
					return null;
				}
			},
			value, title, size, tmp,
			name = talent.name;
			
		if(talent.disc_talent == undefined && !talent.versatility) {
			name += " [Support]";
		}
		title = new Element("span", {
			"text": name
		}).inject(info.container);
		if(talent.disc_talent && !talent.versatility) {
			new Element("span", {
				"class": "talent-disc abs",
				text: "x"
			}).inject(info.container);
		}
		["action", "karma"].each(function(item) {
			if(talent[item]) {
				new Element("span", {
					"class": "abs talent-" + item,
					text: "x"
				}).inject(info.container);
			}
		});
		if(talent.strain) {
			new Element("span", {
				"class": "abs talent-strain",
				text: talent.strain
			}).inject(info.container);
		}
		value = new Element("div", {
			"class": "talent-value abs"
		}).inject(info.container);
		info.rank = new Element("div", {
			"class": "number attribute"
		}).inject(value);
		info.updown = new ed.gui.UpDown(info.rank, {
			onAdd: this.setTalent,
			onSubtract: this.setTalent,
			data: info
		}).setValue(talent.rank);
		info.toggles = new ed.gui.Toggles(info.rank, {
			onChange: this.setTalentFreebie,
			data: info
		}).setValue((talent.freebies || 0));
		if(talent.attribute) {
			info.attribute = ed.Data.getAttributeName(talent.attribute, true);
			new Element("span", {
				"class": "abs talent-attribute",
				text: info.attribute.slice(0,3).toUpperCase()
			}).inject(info.container);
			tmp = talent.rank + this.generator.getAttributeStep(info.attribute) + talent.stepbonus;
			info.step = new Element("span", {
				"class": "abs talent-step",
				text: tmp
			}).inject(info.container);
			info.dice = new Element("span", {
				"class": "abs talent-dice",
				text: this.generator.getStepDice(tmp)
			}).inject(info.container);
		}
		
		tmp = null;
		if(sorted) {			
			this.talents.getChildren().some(function(sibling) {
				var compare = sibling.retrieve("talent").talent;
				if(!compare) {
					return false;
				}
				if(compare.bought < talent.bought) {
					return false;
				}
				if(compare.bought === talent.bought && compare.name < talent.name) {
					return false;
				}
				tmp = sibling;
				return true;
			});
		}
		if(!tmp) {
			info.container.inject(this.talents);
		} else {
			info.container.inject(tmp, "before");
		}
		info.container.store("talent", info);
		
		size = title.getSize();
		if(size.x > 230) {
			title.setStyle("font-size", Math.floor(20 * (230.0 / size.x)) + "px");
		}
		if(option) {
			option.activate(false);
		}
		return info;
	},
	setTalent: function(updown, value, oldValue) {
		var info = updown.data;
		if(!this.generator.setTalent(info.talent.id, value, value < oldValue)) {
			updown.setValue(oldValue);
		} else {
			this.drawTalent(info);
			if(value === 0 && info.talent.rank === 0) {
				info.dispose();
				return;
			}
		}
	},
	setTalentFreebie: function(toggle, value, oldValue) {
		var info = toggle.data;
		if(!this.generator.setTalentFreebie(info.talent.id, value)) {
			toggle.setValue(oldValue);
		} else {
			this.drawTalent(info);
			if(value === 0 && info.talent.rank === 0) {
				info.dispose();
				return;
			}
		}
	},
	addTalent: function(talent, option) {
		if(this.generator.addTalent(talent.id)) {
			this.buildTalent(talent, option, true);
			this.info.write("Free talent ranks: " + this.generator.talents.freebies, "talents");
			this.lpChanged();
			this.checkTalent(talent);
		}
	},
	drawTalent: function(info) {
		if(!info || !info.container) {
			return;
		}
		var step;
		info.updown.setValue(info.talent.rank);
		info.toggles.setValue(info.talent.freebies);
		if(info.attribute) {
			step = info.talent.rank + this.generator.getAttributeStep(info.attribute) + info.talent.stepbonus;
			info.step.set("text", step);
			info.dice.set("text", this.generator.getStepDice(step));
		}
		this.info.write("Free talent ranks: " + this.generator.talents.freebies, "talents");
		this.lpChanged();
		this.checkTalent(info.talent);
	},
	drawTalentsByAttribute: function(attribute) {
		this.talents.getChildren().each(function(container) {
			var info = container.retrieve("talent");
			if(info && info.attribute === attribute) {
				this.drawTalent(info);
			}
		}, this);
	},
	checkTalent: function(talent) {
		if(talent.id === 80) {	// durability
			this.drawCharacteristics();
		}
	},
	buildTalentOption: function(talent, container) {
		var option = {
				element: new Element("div", {
						"class": "option",
						text: talent.name + " [" + (talent.circle || talent.minCircle) + "]"
					}).inject(container),
				activate: function(on) {
					this.element[(on ? "remove" : "add") + "Class"]("inactive");
					return null;
				}
			};
		option.element.addEvent("click", this.addTalent.bind(this, talent, option));
		return option;
	},
	skillFreebiesChanged: function(redraw) {
		if(redraw) {
			this.redraw();
		} else {
			this.info.write("Free general skill ranks: " + this.generator.skills.freebies.general, "skill-general");
			this.info.write("Free artisan skill ranks: " + this.generator.skills.freebies.artisan, "skill-artisan");
			this.info.write("Free knowledge skill ranks: " + this.generator.skills.freebies.knowledge, "skill-knowledge");
			this.info.write("Free language skill ranks: " + this.generator.skills.freebies.language, "skill-language");
			this.lpChanged();
		}
	},
	buildSkillOption: function(skill, container) {
		var option = {
				element: new Element("div", {
						"class": "option",
						text: skill.name
					}).inject(container),
				activate: function(on) {
					this.element[(on ? "remove" : "add") + "Class"]("inactive");
					return null;
				}
			};
		option.element.addEvent("click", this.addSkill.bind(this, skill, option, false));
		return option;
	},
	buildSkill: function(skill, option) {
		var info = {
				container: new Element("div", {
					"class": "skill"
				}),
				option: option,
				skill: skill,
				dispose: function() {
					this.option = this.option.activate(true);
					this.skill = null;
					this.rank = null;
					this.updown = this.updown.dispose();
					this.toggles = this.toggles.dispose();
					this.container.eliminate("skill");
					this.container = this.container.destroy();
					return null;
				}
			},
			value, title, size, tmp,
			name = skill.name,
			typeKey = ed.constants.skillType[skill.type];
		
		if(typeKey !== "general") {
			name += " [" + typeKey.capitalize() + "]";
		}
		title = new Element("span", {
			"text": name
		}).inject(info.container);
		if(skill.action) {
			new Element("span", {
				"class": "abs skill-action",
				text: "x"
			}).inject(info.container);
		}
		if(skill.strain) {
			new Element("span", {
				"class": "abs skill-strain",
				text: skill.strain
			}).inject(info.container);
		}
		value = new Element("div", {
			"class": "skill-value abs"
		}).inject(info.container);
		info.rank = new Element("div", {
			"class": "number attribute"
		}).inject(value);
		info.updown = new ed.gui.UpDown(info.rank, {
			onAdd: this.setSkill,
			onSubtract: this.setSkill,
			data: info
		}).setValue(skill.rank);
		info.toggles = new ed.gui.Toggles(info.rank, {
			onChange: this.setSkillFreebie,
			data: info
		}).setValue(skill.getFreebiesTotal());
		if(skill.attribute) {
			info.attribute = ed.Data.getAttributeName(skill.attribute, true);
			new Element("span", {
				"class": "abs skill-attribute",
				text: info.attribute.slice(0,3).toUpperCase()
			}).inject(info.container);
			tmp = skill.rank + this.generator.getAttributeStep(info.attribute);
			info.step = new Element("span", {
				"class": "abs skill-step",
				text: tmp
			}).inject(info.container);
			info.dice = new Element("span", {
				"class": "abs skill-dice",
				text: this.generator.getStepDice(tmp)
			}).inject(info.container);
		}
		
		info.container.inject(this.skills);
		info.container.store("skill", info);
		
		size = title.getSize();
		if(size.x > 285) {
			title.setStyle("font-size", Math.floor(20 * (285.0 / size.x)) + "px");
		}
		if(option) {
			option.activate(false);
		}
		return info;
	},
	addSkill: function(skill, option, skipMessages) {
		var skill = this.generator.addSkill(skill.id);
		if(skill) {
			this.buildSkill(skill, option);
			if(!skipMessages) {
				this.info.write("Free general skill ranks: " + this.generator.skills.freebies.general, "skill-general");
				this.info.write("Free artisan skill ranks: " + this.generator.skills.freebies.artisan, "skill-artisan");
				this.info.write("Free knowledge skill ranks: " + this.generator.skills.freebies.knowledge, "skill-knowledge");
				this.info.write("Free language skill ranks: " + this.generator.skills.freebies.language, "skill-language");
				this.lpChanged();
			}
		}
	},
	setSkill: function(updown, value, oldValue) {
		var info = updown.data;
		if(!this.generator.setSkill(info.skill.id, value)) {
			updown.setValue(oldValue);
		} else {
			this.drawSkill(info);
			if(value === 0 && info.skill.rank === 0) {
				info.dispose();
				return;
			}
		}
	},
	setSkillFreebie: function(toggle, value, oldValue) {
		var info = toggle.data;
		if(!this.generator.setSkillFreebie(info.skill.id, value)) {
			toggle.setValue(oldValue);
		} else {
			this.drawSkill(info);
			if(value === 0 && info.skill.rank === 0) {
				info.dispose();
				return;
			}
		}
	},
	drawSkill: function(info) {
		if(!info || !info.container) {
			return;
		}
		var step;
		info.updown.setValue(info.skill.rank);
		info.toggles.setValue(info.skill.getFreebiesTotal());
		if(info.attribute) {
			step = info.skill.rank + this.generator.getAttributeStep(info.attribute);
			info.step.set("text", step);
			info.dice.set("text", this.generator.getStepDice(step));
		}
		this.info.write("Free general skill ranks: " + this.generator.skills.freebies.general, "skill-general");
		this.info.write("Free artisan skill ranks: " + this.generator.skills.freebies.artisan, "skill-artisan");
		this.info.write("Free knowledge skill ranks: " + this.generator.skills.freebies.knowledge, "skill-knowledge");
		this.info.write("Free language skill ranks: " + this.generator.skills.freebies.language, "skill-language");
		this.lpChanged();
	},
	drawSkillsByAttribute: function(attribute) {
		this.skills.getChildren().each(function(container) {
			var info = container.retrieve("skill");
			if(info && info.attribute === attribute) {
				this.drawSkill(info);
			}
		}, this);
	},
	drawKarma: function() {
		var key,
			karma = this.generator.getKarma();
		for(key in this.karma) {
			this.karma[key].set("text", karma[key]);
		}
	},
	redraw: function() {
		var key, talentOptions, tmp, skills;
		
		if(this.suppressRedraw) {
			return;
		}
		
		this.races.value = this.generator.race;
		this.fillDisciplines();
		this.disciplines.value = this.generator.discipline;
		this.circle.setValue(this.generator.getCircle());
		this.abilities.set("text", this.generator.getAbilities().join(", "));
	
		// show attributes
		ed.constants.attributes.each(function(key) {
			this.attributes[key].value.setValue(this.generator.getAttribute(key));
			this.attributes[key].increase.setValue(this.generator.getAttributeIncreases(key));
			this.drawAttribute(key);
		}, this);
		this.info.write("Attribute buying points: " + this.generator.attributes.points, "attributes");
		// show karma
		this.drawKarma();
		// clear bought talent list
		this.talents.getChildren().each(function(item) {
			item.retrieve("talent").dispose();
		});
		// build talent options and bought talents
		talentOptions = this.generator.getAllTalents(this.generator.getCircle());
		for(key in talentOptions) {
			tmp = $(key + "_options");
			tmp.empty();
			talentOptions[key].each(function(talent) {
				var option = this.buildTalentOption(talent, tmp);
				if(talent.rank > 0) {
					this.buildTalent(talent, option, true);
				}
			}, this);
		}
		// build skill options
		this.skills.getChildren().each(function(item) {
			item.retrieve("skill").dispose();
		});
		skills = this.generator.getSkillsById();
		key = 0;
		ed.Data.getSkills().each(function(skill) {
			if(skill.type !== key) {
				key = skill.type;
				switch(key) {
					case 1:
						tmp = $("general_options");
						break;
					case 2:
						tmp = $("knowledge_options");
						break;
					case 3:
						tmp = $("artisan_options");
						break;
					case 4:
						tmp = $("language_options");
						break;
				}
				tmp.empty();
			}
			var option = this.buildSkillOption(skill, tmp);
			if(skills[skill.id]) {
				this.addSkill(skill, option, true);
			}
		}, this);		
		// characteristics
		this.drawCharacteristics();
		this.info.write("Free talent ranks: " + this.generator.talents.freebies, "talents");
		this.info.write("Free general skill ranks: " + this.generator.skills.freebies.general, "skill-general");
		this.info.write("Free artisan skill ranks: " + this.generator.skills.freebies.artisan, "skill-artisan");
		this.info.write("Free knowledge skill ranks: " + this.generator.skills.freebies.knowledge, "skill-knowledge");
		this.info.write("Free language skill ranks: " + this.generator.skills.freebies.language, "skill-language");
		this.lpChanged();
	},
	lpChanged: function() {
		this.fireEvent("lpChanged", [this.generator.getLp(), this.generator.getLp(true)]);
	},
	save: function() {
		var state = [],
			content, result;
		state.push(this.options.magic);
		state.push(this.name.get("value") || "");
		state.push(this.generator.save());
		content = JSON.encode(state);
		result = {
			content: content.length + "|" + content,
			filename: state[1]
		};
		return result;
	},
	load: function(content) {
		var state, index, len;
		if(!content) {
			return false;
		}
		ed.console.debug("Load character");
		index = content.indexOf("|");
		if(index == -1) {
			return this.noticeError("Separator missing");
		}
		len = content.slice(0, index).toInt();
		content = content.slice(index + 1).trim();
		ed.console.debug("  -Saved Length: " + len);
		ed.console.debug("  -Actual Length: " + content.length);
		if(!len || typeOf(len) !== "number" || content.length !== len) {
			return this.noticeError("Content length does not match expected count.");
		}
		state = JSON.decode(content, true);
		if(!state || state.length !== 3) {
			return this.noticeError("Invalid char sequence found");
		}
		this.fixOldSaves(state);
		if(state[0] !== this.options.magic) {
			return this.noticeError("Invalid magic token");
		}
		this.name.set("value", state[1] || "");
		if(this.generator.load(state[2])) {
			this.redraw();
			return true;
		}
		return false;
	},
	fixOldSaves: function(state) {
		if(state[0] == 42) {
			state[2].lpTotal = this.generator.options.lp.starting;
			++state[0];
		}
	},
	reset: function() {
		this.generator.reset();
		this.name.set("value", "");
		this.redraw();
	},
	noticeError: function(msg) {
		this.error.notice(msg);
		return false;
	}
});


ed.gui.Tools = new Class({
	Implements: [Options],
	options: {
		win: "tools",
		opener: "tools_open",
		closer: "tools_close",
		closedCss: "tools-closed",
		file: "file",
		saver: "save",
		loader: "load",
		reset: "reset",
		popupName: "ed_save",
		current_lp: "current_lp",
		total_lp: "total_lp",
		change_lp: "change_lp"
	},
	initialize: function(sheet, options) {
		this.sheet = sheet;
		this.setOptions(options);
		this.win = $(this.options.win);
		this.opener = $(this.options.opener).addEvent("click", this.open.bind(this));
		this.closer = $(this.options.closer).addEvent("click", this.close.bind(this));
		this.reset = $(this.options.reset).addEvent("click", this.reset.bind(this));
		this.saver = $(this.options.saver).addEvent("click", this.save.bind(this));
		this.file = $(this.options.file);
		this.loader = $(this.options.loader).addEvent("click", this.load.bind(this));
		
		this.current_lp = $(this.options.current_lp);
		this.total_lp = $(this.options.total_lp);
		$(this.options.change_lp).addEvent("click", this.changeLp.bind(this));
		this.setup();
	},
	setup: function() {
		this.setupLp();
		this.sheet.addEvent("lpChanged", this.redrawLp.bind(this));
	},
	open: function() {
		this.win.removeClass(this.options.closedCss);
	},
	close: function() {
		this.win.addClass(this.options.closedCss);
		if(this.area) {
			this.area = this.area.destroy();
		}
	},
	reset: function() {
		if(window.confirm("This will delete your current character. Do you really want to proceed?")) {
			this.sheet.reset();
			this.close();
		}
		return false;
	},
	save: function() {
		var state = this.sheet.save(),
			form;
		form = new Element("form", {
			action: window.location.protocol + "//www.behindthemirrors.de/php/ED/ed_char_echo.php",
			method: "post",
			styles: {
				display: "none"
			}
		});
		new Element("input", {
			type: "hidden",
			name: "content",
			value: state.content
		}).inject(form);
		new Element("input", {
			type: "hidden",
			name: "filename",
			value: state.filename
		}).inject(form);
		form.inject(document.body).submit();
		form = form.destroy();
		this.close();
	},
	load: function() {
		var file = this.file.files[0],
			reader;
		if(file) {
			if(window.confirm("Do you really want to load a character from this file and overwrite all current data on this page?")) {
				this.close();
				reader = new FileReader();
				reader.onload = function(e) {
					this.sheet.load(e.target.result);
				}.bind(this);
				reader.readAsText(file, "UTF-8");				
			}
		}
	},
	changeLp: function() {
		var lp = this.current_lp.value.toInt();
		if(typeOf(lp) == "number" && lp >= 0) {
			this.sheet.getGenerator().setLp(lp);
			this.sheet.lpChanged();
		} else {
			this.setupLp();
		}
	},
	setupLp: function() {
		var generator = this.sheet.getGenerator();
		this.current_lp.value = generator.getLp();
		this.total_lp.set("text", generator.getLp(true));
	},
	redrawLp: function(lp, total) {
		this.current_lp.value = lp;
		this.total_lp.set("text", total);
	}
});